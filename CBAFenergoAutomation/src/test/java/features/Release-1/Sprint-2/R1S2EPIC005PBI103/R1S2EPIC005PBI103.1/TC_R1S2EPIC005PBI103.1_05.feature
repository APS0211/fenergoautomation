#Test Case: TC_R1S2EPIC005PBI103.1_05
#PBI: R1S2EPIC005PBI103.1
#User Story ID: FIG3, FIG5,FIG6, FIG7, FIG8, FIG9
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103.1_05

  @Tobeautomated
  Scenario: Verify the following functionality on Document details screen for Onboarding Maker
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "NBFI"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify adding a new document corresponding to document requirement
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    When I add document by browsing a document from "UploadDocument" button
    When I add "DocumentName" and "Documentcategory" and click on "Save" button
    Then I can see document is added successfully
    When I navigate back to "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify adding an existing document corresponding to document requirement
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    When I select "Existingdocument" from "Document source" drop-down
    When I enter id of already existing document in "DocumentID" field and click on "search" button
    When I select a document from the results and click on "save" button
    When I expand the grid corresponding to the same document requirement
    Then I can see existing document is added successfully to the document requirement
    # Test-Data: Verify adding additional document corresponding to document requirement
    When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
    When I navigate to "DocumentDetails" screen
    When I add document by browsing a document from "UploadDocument" button
    When I add "DocumentName" and "Documentcategory" and click on "Save" button
    Then I can see document is added successfully
    When I navigate back to "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify User is able to view attached document corresponding to document requirement on "Document details" screen
    When I click on "View" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    Then I can see document is already attached on "DocumentDetails" screen along with "DocumentName" and "Documentcategory"
    When I navigate back to "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify User is able to view attached document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
    Then I can see attached document is displaying in document grid as well corresponding to document requirement
    When I navigate back to "KYCDocumentRequirementsGrid" task
    When I navigate to "DocumentDetails" screen
    When I expand the Document requirement on "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify User is able to view pending document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
    Then I can see "Status" of document is displaying as "pending" in document grid
    When I expand the Document requirement on "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify User is able to remove attached document corresponding to document requirement on "Document details" screen
    When I click on "remove" from options button displaying corresponding to document requirement having a document attached to it
    Then I can see attached document is removed which was attached to the document requirement displaying under document grid
    #Test-data: Verify re-uploading a document after removing the document corresponding to document requirement on "Document details" screen
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    When I add document by browsing a document from "UploadDocument" button and complete the task
    And I add document again and see document is added successfuly after removing document
    # Test-Data: Verify User is not able to proceed further after removing the document attached document corresponding to mandatory document requirement on "Document details" screen
    When I see document is removed corresponding to mandatory document requirement
    Then I click on "saveandcomplete" button to complete the task
    Then I can see notificatin at the top of the screen stating complete mandatory document requirement
    And I can see user is not able to complete the task without fulfilling mandatory document requirement
    When I navigate back to "KYCDocumentRequirementsGrid" task
    # Test-Data: Verify adding "Email approval" feature corresponding to document requirement
    When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
    When I navigate to "DocumentDetails" screen
    When I add document by browsing a document from "Requestviaemail" button
    When I add "contact" in the Contact grid.
    And I select added contact and click on "EditEmail" button
    When I navigate to "EditEmail" screen and complete the task
    Then I can see Email is sent successfully
		#Test-data: verify "Others" as document requirement on requirement grid of "KYC document requirement" screen
    When I navigate back to "KYCDocumentRequirementsGrid" task
    Then I can see "Others" as document requirement on requirement grid
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
