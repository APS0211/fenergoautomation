#Test Case: TC_R1S2EPIC005PBI103.2_02
#PBI: R1S2EPIC005PBI103.2
#User Story ID: FIG10, FIG11, FIG12, FIG13, FIG14, FIG15
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI103.2_02

  @Tobeautomated
  Scenario: Verify New added Fields and Grids added on "Edit Verification" task of "Complete ID&V" task for "AML"  stage
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "FI"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Right click on hologram to Add Associated Party and add "Non-Individual" type Association and save it
    When I complete "CompleteAMLGrid" task
    When I navigate to "CompleteID&VGrid" task
    Then I can see already added association under "Associatedparties" section
    When I click on "Edit" button displaying corresponding to the added association
    When I navigate to "EditVerification" task
    Then I can see below mentioned Newly added Fields and grids on LE details on "EditVerification" task
    And I verify below fields under "LE details" section
      | Legal Entity Name                       |
      | Client Type                             |
      | Legal Entity Type                       |
      | Country of Incorporation/ Establishment |
      | Country of Domocile/ Physical Pesence   |
      | LEI                                     |
      | Name of Registration Body               |
    And I verify below fields under	"Entity Under Verification" section
      | ID&V Category |
    And I verify below fields underGrids
      | Addresses      |
      | Documents      |
      | Tax Identifier |

