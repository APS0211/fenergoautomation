#Test Case: TC_R1S2EPIC006PBI002_01
#PBI: R1S2EPIC006PBI002
#User Story ID: 5,7
#Designed by: Anusha PS
#Last Edited by: Priyanka Arora
#Note: The same test data can be used for first 3 test cases of this PBI.
@Automation
Feature: Screening

  Scenario: Validate metadata and LOVs for the fields under "Adverse Media" and "Sanctions" in Complete AML task - Assessment - Fircosoft Screening
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    #Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
    And I add Fircosoft Screening for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    And I navigate to "Assessment" screen
    #To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
    And I navigate to "Fircosoft Screening" screen
    #Validation for Adverse Media
    And I validate "Adverse Media" is the first section under "Fircosoft Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the only following field in "Adverse Media" section
      | Fenergo Label Name   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Adverse Media Status | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "Adverse Media Status" field
      | No Match       |
      | False Match    |
      | Positive Match |
    And I validate 'Adverse Media	 Category' is not visible
    And I select "Positive Match" for "Adverse Media Status" field
    And I validate 'Adverse Media Category' is visible
    And I validate the conditional field in "Adverse Media" section
      | Fenergo Label Name     | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | Adverse Media Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "Adverse Media Category" field
    #Refer PBI for LOV list
    And I select "No Match" for "Adverse Media Status" field
    And I validate 'Adverse Media Category' is not visible
    #Validation for Sanctions
    And I validate "Sanctions" is the Third section under "Fircosoft Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the only following field in "Sanctions" section
      | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Sanctions Status   | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "Sanctions Status" field
      | No Match       |
      | False Match    |
      | Positive Match |
    And I validate 'Sanctions Category' is not visible
    And I select "Positive Match" for "Sanctions Status" field
    And I validate 'Sanctions Category' is visible
    And I validate the conditional field in "Sanctions" section
      | Fenergo Label Name | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | Sanctions Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "Sanctions Category" field
    #Refer PBI for LOV list
    And I select "No Match" for "Sanctions Status" field
    And I validate 'Sanctions Category' is not visible
