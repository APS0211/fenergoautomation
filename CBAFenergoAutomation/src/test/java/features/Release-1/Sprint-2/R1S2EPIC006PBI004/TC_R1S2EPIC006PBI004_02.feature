#Test Case: TC_R1S2EPIC006PBI004_02
#PBI: R1S2EPIC006PBI004
#User Story ID: 13
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Screening

  Scenario: Validate if system is automatically pick up the values that are recorded in both Fircosoft and Google screening whereby there is a 'Positive Match' 
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    And Ficrosoft screening for the entity with "Positive" match for "Adverse Media"
    And Google screening for the entity with "Positive" match for "Adverse Media"
    And validate if "Adverse Media" field has value "1" in "Complete AML" screen / "Confirmed Matches" section