#Test Case: TC_R1S2EPIC002PBI200_07
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_07

  @To_be_automated
  Scenario: Validate the renamed fields on "LE details" screen for the Associated LE added via Express addition on "Capture Hierarchy details" screen
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as other than "Individual" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields are renamed  on "LE details" screen
    And I verify below renamed fields on "LE details" screen:
    
		Pevious Label                 |  Renamed label
		Principal Place of Business   |  Country of Business Operations/Economic Activity  
		Registration Body             |  Name of Registration Body
		Regulated By                  |  Name of Regulatory Body
		Date of Establishment         |  Date of Incorporation/Establishment
		ISIC                          |  Primary Industry of Operation 