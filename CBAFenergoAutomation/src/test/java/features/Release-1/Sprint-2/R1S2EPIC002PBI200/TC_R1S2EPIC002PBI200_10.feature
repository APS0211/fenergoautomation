#Test Case: TC_R1S2EPIC002PBI200_10
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_10


  @To_be_automated
  Scenario: Validate new fields on "LE details" screen for the Associated LE added via Express addition on "Capture Hierarchy details" screen
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill "Countryofincorporation" as "UAE"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type other than "Individual" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE an click on Navigate to legal Entity
    #Testdata: Verify "ResidentStatus" field is set to "Resident" and displaying as Mandatory on "LE details" screen
    And I Verify "ResidentStatus" field is set to "Resident" and displaying as Mandatory on "LE details" screen
    
Scenario: Validate new fields on "LE details" screen for the Associated LE added via Express addition on "Capture Hierarchy details" screen
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill "Countryofincorporation" other than "UAE"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type other than "Individual" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "ResidentStatus" field is set to "Non-resident" and displaying as non-Mandatory on "LE details" screen
    And I verify "ResidentStatus" field is set to "Non-resident" and displaying as non-Mandatory on "LE details" screen
    
 Scenario: Validate conditionally mandatory "Shareholding %" field on "LE details" screen for the Associated LE for the following Association types
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type ither than "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Ultimate Beneficial Owner (UBO)" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "Shareholding %" field as mandatory for relationship as "Ultimate Beneficial Owner (UBO)" on "LE details" screen
    And I verify "Shareholding %" field as mandatory  on "LE details" screen
    
 Scenario: Validate conditionally mandatory "Shareholding %" field on "LE details" screen for the Associated LE for the following Association types
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type ither than "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investment manager" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "Shareholding %" field as mandatory for relationship as "Investment manager" on "LE details" screen
    And I verify "Shareholding %" field as mandatory  on "LE details" screen   
    
  Scenario: Validate conditionally mandatory "Shareholding %" field on "LE details" screen for the Associated LE for the following Association types
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type ither than "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investor" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "Shareholding %" field as mandatory for relationship as "Investor" on "LE details" screen
    And I verify "Shareholding %" field as mandatory  on "LE details" screen   

 Scenario: Validate conditionally mandatory "Shareholding %" field on "LE details" screen for the Associated LE for the following Association types
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type ither than "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Settlor" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "Shareholding %" field as mandatory for relationship as "Settlor" on "LE details" screen
    And I verify "Shareholding %" field as mandatory  on "LE details" screen   
           
 Scenario: Validate conditionally mandatory "Shareholding %" field on "LE details" screen for the Associated LE for the following Association types
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type ither than "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Shareholder" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify "Shareholding %" field as mandatory for relationship as "Shareholder" on "LE details" screen
    And I verify "Shareholding %" field as mandatory  on "LE details" screen   
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
