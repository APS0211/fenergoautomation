#Test Case: TC_R1S2EPIC002PBI200_03
#PBI: R1S2EPIC002PBI200
#User Story ID: US012
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_03

  @To_be_automated
  Scenario: Validate conditionally mandatory fields on "LE details" screen for the Associated LE for the following Association types and client type "Corporate"
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Authorised Signatory" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Authorised Signatory" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation

		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investment Manager" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investment Manager" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Custodian" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Custodian" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Power of Attorney" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Power of Attorney" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Settlor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Settlor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Shareholder" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Shareholder" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Ultimate Beneficial Owner (UBO)" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Ultimate Beneficial Owner (UBO)" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Legal Representative" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Legal Representative" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
			
 Scenario: Validate conditionally mandatory fields on "LE details" screen for the Associated LE for the following Association types and client type "PCG Entity"
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "PCG Entity" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Authorised Signatory" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Authorised Signatory" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation

		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investment Manager" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investment Manager" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Custodian" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Custodian" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Investor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Investor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Power of Attorney" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Power of Attorney" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Settlor" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Settlor" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Shareholder" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Shareholder" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Ultimate Beneficial Owner (UBO)" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Ultimate Beneficial Owner (UBO)" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
		
		When I nvaigate to "CaptureHierarchydetails" task 
		When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type as "Individual"
    When I navigate to "Associationdetails" task
    Then I select relationship as "Legal Representative" and complete th task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as mandatory for relationship as "Legal Representative" on "LE details" screen
    And I verify below mentioned fields as mandatory  on "LE details" screen:
    
		-Date of Birth
		-Nationality
		-Place of Birth
		-Occupation	
				
						
		