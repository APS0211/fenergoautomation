#Test Case: TC_R1S2EPIC002PBI200_08
#PBI: R1S2EPIC002PBI200
#User Story ID: US011
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_08

  @To_be_automated
  Scenario: Validate the hidden fields on "LE details" screen for the Associated LE added via Express addition on "Capture Hierarchy details" screen
  	Given I login to Fenergo Application with "RM"
	  When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	  And I fill the data for "CaptureNewRequest" with key "C1"
	  And I click on "Continue" button
	  When I complete "CaptureRequestDetailsFAB" task
	  When I complete "ReviewRequest" task
	  Then I store the "CaseId" from LE360
	  When I login to Fenergo Application with "OnBoardingMaker"
	  When I search for "Caseid"
	  When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I Complete "EnrichKYCProfileGrid" task
    When I navigate to "CaptureHierarchydetails" task
    When I right click on Hologram and select Associated party
    Then I navigate to "AssociatedParties" task 
    When I select Legal Entity type other than "Individual" and complete the task
    When I navigate to "CaptureHierarchydetails" task again
    When I Right click on hologram of above added associated LE and click on Navigate to legal Entity
    #Testdata: Verify below mentioned fields as hidden  on "LE details" screen
    And I verify below mentioned fields as hidden under following sections on "LE details" screen:
    
		"Customer details" section
			-Australian Company Number Issued
			-Registered by Australian Securities and Investment Commission
			-Legal Status
			-Anticipated Activity of Account
			-Date of Establishment
			-Location of Business/Activity

		"Industry codes details" section
			-SIC
			-NAIC
			-ISC
			-ISIN
			-NACE 2 Code
			-Stock Exchange Code
			-Central Index Key
			-SWIFT BIC
    