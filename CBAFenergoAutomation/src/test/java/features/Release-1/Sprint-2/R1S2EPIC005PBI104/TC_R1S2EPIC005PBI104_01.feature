#Test Case: TC_R1S2EPIC005PBI104_01
#PBI: R1S2EPIC005PBI104
#User Story ID: CORP3, CORP5, CORP6, CORP7, CORP8
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S2EPIC005PBI104_01

@To be automated

Scenario: Verify the following functionality on Document details screen for Onboarding Maker
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "Corporate"
	When I navigate to "CaptureRequestDetailsFAB" task 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	
	# Test-Data: Verify uploading document corresponding to document requirement
	When I click on "AttachDocument" from options button displaying corresponding to document requirement
	When I navigate to "DocumentDetails" screen
	When I add document by browsing a document from "UploadDocument" button
	When I add "DocumentName" and "Documentcategory" and click on "Save" button
	Then I can see document is added successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify adding additional document corresponding to document requiremen and Saving it
	When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
	When I navigate to "DocumentDetails" screen
	When I add document by browsing a document from "UploadDocument" button
	When I add "DocumentName" and "Documentcategory" and click on "Save" button
	Then I can see document is added successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify adding "Document Collection from RM - Email approval" corresponding to document requirement
	When I click on "AttachDocument" from options button displaying corresponding to document requirement having a document attached to it
	When I navigate to "DocumentDetails" screen
	When I add document by browsing a document from "Requestviaemail" button
	When I add "contact" in the Contact grid.
	And I select added contact and click on "EditEmail" button 
	When I navigate to "EditEmail" screen and complete the task 
	Then I can see Email is sent successfully
	
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to view attached document corresponding to document requirement on "Document details" screen
	When I click on "View" from options button displaying corresponding to document requirement
	When I navigate to "DocumentDetails" screen
	Then I can see document is already attached on "DocumentDetails" screen along with "DocumentName" and "Documentcategory"
	When I navigate back to "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to view attached document corresponding to document requirement on "KYCDocumentRequirementsGrid" screen
	Then I can see attached document is displaying in document grid as well corresponding to document requirement
	
	When I expand the Document requirement on "KYCDocumentRequirementsGrid" task
	# Test-Data: Verify User is able to remove attached document corresponding to document requirement on "Document details" screen
	When I click on "remove" from options button displaying corresponding to document requirement having a document attached to it
	Then I can see attached document is removed which was attached to the document requirement displaying under document grid
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
