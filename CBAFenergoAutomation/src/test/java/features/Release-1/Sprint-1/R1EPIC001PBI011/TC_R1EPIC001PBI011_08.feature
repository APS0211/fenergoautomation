#Test Case: TC_R1EPIC01PBI011_08
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@R1EPIC01PBI011 
Scenario:
"Accounts" subflow is displaying as hidden on "Enrich KYC Profile" task on "Enrich Client information" stage 
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	And I store the "CaseId" from LE360 
	When I complete "ReviewRequest" task 
	Given I login to Fenergo Application with "OnboardingMaker" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
	When I navigate to "EnrichKYCProfileGrid" task 
	Then I can see "Accounts" subflow is hidden 
	