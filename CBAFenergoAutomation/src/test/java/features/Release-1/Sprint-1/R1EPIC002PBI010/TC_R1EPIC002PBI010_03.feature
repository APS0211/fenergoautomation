#Test Case: TC_R1EPIC002PBI010_03
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task - Anticipated Transaction Sub Flow

Scenario: Validate if "Anticipated Transaction Sub Flow" subflow is available in "Enrich KYC profile" task for "NBFI" entity and validate the metadata of the subflow / fields

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "NBFI" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "NBFI" and LegalEntityrole as "Client/Counterparty"  
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task 
	And I assert that "Anticipated Transactional Activity (Per Month)" subflow is available in "Enrich KYC profile" screen
	And I assert that "Anticipated Transactional Activity (Per Month)" subflow is present above "Comment" section
	And I validate the following fields in "Anticipated Transaction" Sub Flow
		Fenergo Label Name		|Label Sequence	|Field Type							|Visible	|Editable	|Mandatory	|Field Defaults To 	|
		ID										|1							|Alphanumeric						|Yes			|No				|No					|NA									|
		Inward Amount(AED)		|2							|Numeric								|Yes			|Yes			|No					|NA                 |
		Outward Amount				|3							|Numeric								|Yes			|Yes			|No					|NA                	|
		Currencies Involved		|4							|Multiselect drop-down	|Yes			|Yes			|No					|Select...         	|
		Countries Involved  	|5							|Multiselect drop-down	|Yes			|Yes			|No					|Select...         	|
		NBFI							  	|6							|Drop-down							|Yes			|Yes			|No					|Select...         	|
	And I validate the following buttons # from left to right
		CANCEL | SAVE & ADD ANOTHER  | SAVE 
	And I validate "Currencies Involved" drop-down has following LOVs
		#List from OOTB
	And I validate "Countries Involved" drop-down has following LOVs
		#Refer PBI (247 countries)	
	And I validate "NBFI" drop-down has following LOVs
		|Cash Management   |
		|Trade Finance     |
		|Corporate Finance |
		|Global Markets    |

