#Test Case: TC_R1EPIC002PBI010_12
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task - Anticipated Transaction Sub Flow

Scenario: Validate if Onboarding Maker is able to cancel Anticipated Transaction "PCG" entity type  &
					Validate if Onboarding Maker is able to add Anticipated Transaction after getting referred back


	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "PCG" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "PCG" and LegalEntityrole as "Client/Counterparty"  
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task 
	
	#Navigate to Anticipated Transactional Activity (Per Month) subflow using "+" button
	And I navigate to "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I enter data in the screen
	And I click Cancel button
	
	#On clicking Cancel button OnboardingMaker should be taken back to "Enrich KYC profile" screen
	And I assert that user is back to "Enrich KYC profile" screen
	
	When I complete "EnrichKYCProfileFAB" task
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task  
	
	And I refer back to 'Enrich Client Information stage
	
	When I navigate to "EnrichKYCProfileGrid" task 
	
	And I add "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the edited transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Value of Projected Transactions (AED) | Number of Transactions  | Type/Mode of Transactions | Transaction Type | Currencies Involved | Countries Involved|  

	When I complete "EnrichKYCProfileFAB" task 