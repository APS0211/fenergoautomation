#Test Case: TC_R1EPIC002PBI010_06
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich KYC profile task - Anticipated Transaction Sub Flow

Scenario: Validate if Onboarding Maker is able to add/edit/delete Anticipated Transaction and save for "NBFI" entity type
					Validate if Onboarding Maker is able to save and complete the "Enrich KYC Profile" task with out adding Anticipated Transaction

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "NBFI" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "NBFI" and LegalEntityrole as "Client/Counterparty"  
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task 
	
	And I add a "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the added transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Inward Amount(AED) | Outward Amount	| Currencies Involved | Countries Involved | NBFI |
	
	And I edit "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the edited transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Inward Amount(AED) | Outward Amount	| Currencies Involved | Countries Involved | NBFI |  
	
	And I delete "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the grid "Anticipated Transactional Activity (Per Month)" is empty
	
	#OnboardingMaker should be able to save and complete the task without adding Anticipated Transactional Activity (Per Month)
	When I complete "EnrichKYCProfileFAB" task 