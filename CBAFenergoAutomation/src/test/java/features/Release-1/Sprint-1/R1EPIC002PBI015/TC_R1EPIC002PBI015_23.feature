#Test Case: TC_R1EPIC002PBI015_23
#PBI: R1EPIC002PBI015
#User Story ID: US114
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate LOVs for "Legal Entity Type" field on "Search for Duplicates" screen of "New request" stage 
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	When I navigate to "Search for Duplicates" screen by clicking on "search" button
	#Test Data: verify LOvs for "Legal Entity Type" field on "Search for Duplicates" screen
	Then I verify LOVs for "Legal Entity Type" field (sequence,LOV,Visibility/editability/mandatory) on "Search for Duplicates" screen