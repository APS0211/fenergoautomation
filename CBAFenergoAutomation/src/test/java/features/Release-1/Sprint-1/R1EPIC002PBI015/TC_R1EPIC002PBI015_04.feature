#Test Case: TC_R1EPIC002PBI015_04
#PBI: R1EPIC002PBI015
#User Story ID: US112
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation2
Scenario: Validate "FAB Entity Type" label is renamed as "Client Type" label on "New request" stage 
	Given I login to Fenergo Application with "RM"
	When I click on "PlusButton" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	#Test Data:"FAB Entity Type" label should be renamed as "Client Type" label on "Completerequest" screen of "New request" stage
	Then I can see "FABEntityType" label is renamed as "ClientType" on EnterEntitydetails screen