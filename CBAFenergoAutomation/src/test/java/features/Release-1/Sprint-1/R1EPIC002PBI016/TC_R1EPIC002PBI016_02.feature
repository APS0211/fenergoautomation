#Test Case: TC_R1EPIC002PBI016_02
#PBI: R1EPIC002PBI016
#User Story ID: USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Internal Booking Entity section

@Tobeautomated @TC_R1EPIC002PBI016_02
Scenario: Validate the removed fields under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "C1" and below data
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button	
	And I check that below data is not visible
	|FieldLabel|
	|Priority|
	|From Office Area|
	|On Behalf Of|
	|Internal desk|
	#Test Data:Verify "Priority","From Office Area","On Behalf Of","Internal desk" drop-doenfields should be display as removed on "Review request" screen	
#	Then I can see "Priority","From Office Area","On Behalf Of","Internal desk" drop-down fields displaying as removed