#Test Case: TC_R1EPIC002PBI016_07
#PBI: R1EPIC002PBI016
#User Story ID: US055,US056,US057, US058, US059
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: Internal Booking Entity section

@To be automated 

Scenario: Validate LOVs for "Target Code", "Sector Description" drop-down under "Internal Booking details" section on "Capture request details" screen for RM user
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	#Test-data: Verify LOVs for "Target Code", "Sector Description" dropdown under "internal Booking details" section
	# Refer PBI for LOvs for "Target Code", "Sector Description" drop-down
	Then I verify "Target Code", "Sector Description" drop-down