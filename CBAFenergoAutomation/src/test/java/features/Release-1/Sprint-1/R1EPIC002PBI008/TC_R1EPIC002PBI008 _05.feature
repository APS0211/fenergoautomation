#Test Case: TC_R1EPIC002PBI008_05
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details - Complete Legal Entity Category

@Automation
Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (11) in Validate KYC and Regulatory data screen when 'Client Type' is selected as 'Corporate' from the Enter entity details screen (Refer lov in the PBI)

	Given I login to Fenergo Application with "RM" 
	#Select client Type as 'Corporate' in Enter Entity details screen
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "OnboardingMaker" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
   
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
