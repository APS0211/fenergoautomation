#Test Case: TC_R1EPIC002PBI008_07
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details - Complete Legal Entity Category


@Automation
Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (15) in Validate KYC and Regulatory data screen when 'Client Type' is selected as 'FI' from the Enter entity details screen (Refer lov in the PBI)

	Given I login to Fenergo Application with "RM" 
	#Select client Type as 'FI' in Enter Entity details screen
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	Then I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
  #Validate the Legal Entity Category lovs (Refer lov from the PBI) 
	Then I verify "Legal Entity Category" drop-down values with ClientType as "Financial Institution (FI)"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
