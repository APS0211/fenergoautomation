#Test Case: TC_R1EPIC002PBI008_09
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Capture New Request Details - Complete Legal Entity Category


@Automation
Scenario: Verify the field behavior of 'Legal Entity Category' field in Enrich KYC Profile screen and value is defaulted from complete screen/Validate KYC and Regulatory Data screen

	Given I login to Fenergo Application with "RM" 
	#Select client Type as 'NBFI' in Enter Entity details screen
	When I create a new request with FABEntityType as "Non-Bank Financial Institution (NBFI)" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "OnboardingMaker"
	Then I search for the "CaseId"
     When I navigate to "ValidateKYCandRegulatoryGrid" task
  #Validate the Legal Entity Category lovs (Refer lov from the PBI) 
	Then I verify "Legal Entity Category" drop-down values with ClientType as "Non-Bank Financial Institution (NBFI)"
	When I complete "ValidateKYCandRegulatoryFAB" task 
    When I navigate to "EnrichKYCProfileGrid" task 
    And I check that "LegalEntityCategory" is readonly
	#Verify the Legal entity category value is defaulted from complete screen/Validate KYC and Regulatory Data
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
