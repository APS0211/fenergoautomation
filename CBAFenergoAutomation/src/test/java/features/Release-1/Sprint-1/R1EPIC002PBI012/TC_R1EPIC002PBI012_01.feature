#Test Case: TC_R1EPIC002PBI012_01
#PBI: R1EPIC002PBI012
#User Story ID: US002
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Case Status - KYC Approval Date

  Scenario: 
    Validate if user is able to see "KYC Approval Date"  field in the following screens/section as per mock up screens (for open case) for all LE types and the value for the field is defaulted to "-"
    #LE 360 / LE Overview
    #LE Details / LE Overview (Customer Details)
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I create new request with ClientEntityType as "Corporate" and LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Then I navigate to LE360 screen
    And I validate the following fields in LE360 / LE Overview
      | Fenergo Label Name | Label Sequence       | Field Type | Visible | Editable | Mandatory | Value |
      | KYC Approval Date  | as per mockup screen | Date       | No      | No       | No        | -     |
    And I validate the following fields in LE Details / LE Overview (Customer Details)
      | Fenergo Label Name | Label Sequence       | Field Type | Visible | Editable | Mandatory | Value |
      | KYC Approval Date  | as per mockup screen | Date       | No      | No       | No        | -     |
	#Validate the same for all roles
