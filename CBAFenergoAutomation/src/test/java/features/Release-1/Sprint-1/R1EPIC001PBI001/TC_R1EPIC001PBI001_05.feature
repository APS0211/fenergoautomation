#Test Case: TC_R1EPIC01PBI001_05
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_05

@Automation @TC_R1EPIC01PBI001_05
Scenario:
Validate if various users are able to see "Product Status" as a column on the "Products grid view" in "Capture Request Details", "Review Request", "ValidateKYCandRegulatory" and  "Enrich KYC Profile" screens. 

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Then I login to Fenergo Application with "OnboardingMaker" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1"
	And I click on "SaveandCompleteforValidateKYC" button
	When I navigate to "EnrichKYCProfileGrid" task
	When I complete "AddAddress" task
	And I complete "EnrichKYC" screen with key "C1"
   And I click on "SaveandCompleteforEnrichKYC" button
	#Complete the COB flow
	Given I login to Fenergo Application with "OnboardingChecker" 
	#Search for the caseid created by RM
	And I search for the "CaseId"
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails" 
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails" 
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails" 
	And I navigate to "ReviewRequestGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ReviewRequest" 
	And I assert "Status" column is visible in the Product Grid for "ReviewRequest" 
	And I assert "Status" column is not present as a field below the grid for "ReviewRequest" 
	
	Then I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ValidateKYCRequest" 
	And I assert "Status" column is visible in the Product Grid for "ValidateKYCRequest" 
	And I assert "Status" column is not present as a field below the grid for "ValidateKYCRequest" 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "EnrichKYCProfile" 
	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile" 
	
	
	Given I login to Fenergo Application with "FLoydKYC" 
	And I search for the "CaseId"
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails" 
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails" 
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails" 
	And I navigate to "ReviewRequestGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ReviewRequest" 
	And I assert "Status" column is visible in the Product Grid for "ReviewRequest" 
	And I assert "Status" column is not present as a field below the grid for "ReviewRequest" 
	
	Then I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ValidateKYCRequest" 
	And I assert "Status" column is visible in the Product Grid for "ValidateKYCRequest" 
	And I assert "Status" column is not present as a field below the grid for "ValidateKYCRequest" 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "EnrichKYCProfile" 
	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile" 
	Given I login to Fenergo Application with "Admin" 
	And I search for the "CaseId"
	#Search for the caseid created by RM
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails" 
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails" 
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails" 
	And I navigate to "ReviewRequestGrid" task 
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "ReviewRequest" 
	And I assert "Status" column is visible in the Product Grid for "ReviewRequest" 
	And I assert "Status" column is not present as a field below the grid for "ReviewRequest"  
	#	Given I login to Fenergo Application with "FLODAVP" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	
#	Given I login to Fenergo Application with "Business Unit Head (N3)" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Given I login to Fenergo Application with "FLOD VP" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Given I login to Fenergo Application with "FLOD SVP" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Given I login to Fenergo Application with "Business Head (N2)" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	
#	Given I login to Fenergo Application with "Compliance" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Given I login to Fenergo Application with "Fenergo - Admin" 
#	#Search for the caseid created by RM
#	And I navigate to "CaptureRequestDetails" task s
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ReviewRequest" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "ValidateKYCandRegulatory" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	Then I navigate to "EnrichKYCProfileSection" task 
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid 
#	And I assert "Status" column is not present as a field below the grid 
#	
#	
#	
	
	
	
	