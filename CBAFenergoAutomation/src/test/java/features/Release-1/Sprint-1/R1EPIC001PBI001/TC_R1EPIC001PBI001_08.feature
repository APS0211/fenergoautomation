#Test Case: TC_R1EPIC01PBI001_08
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_08

@Onboarding  @TC_R1EPIC01PBI001_08
Scenario: To Verify Onboarding Maker is able to remove product from action button in "ValidateKYC and Regulatory" and "Enrich KYC Profile" screens

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	 And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	
	Then I login to Fenergo Application with "OnboardingMaker"
	And I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I delete the Product from "ValidateKYCRequest"
	And I assert ProductSection is empty in "ValidateKYCRequest"
	#Add product again
	And I add a Product from "ValidateKYCRequest"
	And I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button
		 
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
