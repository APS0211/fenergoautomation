#Test Case: TC_R1EPIC01PBI001_06
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_06 

@TC_R1EPIC01PBI001_06
Scenario: Validate if various users are able to see "Product Status" as a column on the "Products grid view" in LE360 screen

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	#Adding product with status as "New"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	Then I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	#Complete the COB flow
	When I complete "ValidateKYC" screen with key "C1"
	And I click on "SaveandCompleteforValidateKYC" button
	
	Given I login to Fenergo Application with "FlodAVP" 
	When I search for the "CaseId"
	#Search for the caseid created by RM
	When I navigate to "LE360overview" screen
	And I click on "ProductLE360" from LHS
	When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "CaptureRequestDetails" 
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails" 
