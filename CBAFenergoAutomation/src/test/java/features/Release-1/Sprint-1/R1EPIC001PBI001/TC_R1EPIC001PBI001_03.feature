#Test Case: TC_R1EPIC01PBI001_03
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Products Section- Grid View  
	
@COB @Manual
Scenario: Validate if Onboarding Maker is able to see "Product Status" as a column on the "Products grid view" in "Enrich KYC Profile" screen after adding the Product

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "NFI" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"  
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1| 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	Then I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYCandRegulatoryFAB" task
	When I navigate to "EnrichKYCProfileGrid" task
	And I add a Product from "EnrichKYCProfile"
	
	
	#Adding product with status as "New"
#	And I add a Product with Product Status as "New"
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
	