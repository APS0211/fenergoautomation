#Test Case: TC_R1EPIC01PBI001_04
#PBI: R1EPIC01PBI001
#User Story ID: US014
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC01PBI001_04  

@COB @Automation
Scenario: Validate if Onboarding Maker is able to see "Product Status" as a column on the "Products grid view" in "Enrich KYC Profile" screen after editing a Product (for various product statuses except 'Cancelled') that RM has added in "Capture Request Details" screen
             and Validate the behaviour of "Products grid view" in "Enrich KYC Profile" screen after deleting all the products. Product section should be empty

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "PCG" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	#Adding product with status as "New"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	Then I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "EnrichKYCProfile" 
	And I assert "Status" column is visible in the Product Grid for "CaptureRequestDetails"
	And I assert "Status" column is not present as a field below the grid for "CaptureRequestDetails"
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    #Editing product with status as "Active" 
    And I edit a Product with Product Status as "Active" for "EnrichKYCProfile"
    When I expand "ProductGridExpand" and zoom out so that grid gets fully visible for "EnrichKYCProfile"
	And I assert "Status" column is visible in the Product Grid for "EnrichKYCProfile" 
	And I assert "Status" column is not present as a field below the grid for "EnrichKYCProfile"
	
	
	
	
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
#	#Editing product with status as "Approved"
#	And I edit a Product with Product Status as "Approved"
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
#	#Editing product with status as "Pending Approval"
#	And I edit a Product with Product Status as "Pending Approval"
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
#	#Editing product with status as "Rejected"
#	And I edit a Product with Product Status as "Rejected"
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
#	#Editing product with status as "New"
#	And I edit a Product with Product Status as "New"
#	And I assert "Status" column is visible between "FundCode" and "Actions" in the Product Grid
#	And I assert "Status" column is not present as a field below the grid 
#	
#	#Deleting the added product
#	And I delete the product
#	
#	#Validate if product section is empty
#	And I assert "Product Section" is empty