#Test Case: TC_R1EPIC002PBI017.1_01
#PBI: R1EPIC002PBI017.1
#User Story ID:US082, US112, US099, US101, US027
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.1_01

  @Tobeautomated
  Scenario: Validate the renamed fields under "Customer details" section on "LE360-overview" screen for RM user
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "CaptureRequestDetailsFAB" task
    Then I navigate to "LE360-overview" task
    #Test Data:Verify renamed fields under"Customer details" section
    And I validate the following renamed fields under "Customer details" section
      | Current Label               | Renamed label                                      |
      | Principal Place of Business | Countries of Business Operations/Economic Activity |
      | FAB Entity Type             | Client Type                                        |
      | Registration body           | Name of Registration body                          |
      | Country of Domicile         | Country of Domicile/Physical Presence              |
      | Operating/Trading Name      | Trading/Operation Name                             |
