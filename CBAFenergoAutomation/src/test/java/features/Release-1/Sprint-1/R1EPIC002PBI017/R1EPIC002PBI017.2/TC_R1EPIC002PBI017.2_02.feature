#Test Case: TC_R1EPIC002PBI017.2_02
#PBI: R1EPIC002PBI017.2
#User Story ID:US055, US056, US057, US059
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.2_02

  @Tobeautomated
  Scenario: Validate new auto-populated fields under "Internal Booking details" section on "LE360-overvie" screen for RM user
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "NBFI"
    When I navigate to "CaptureRequestDetailsFAB" task
    Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task
    When I navigate to "QualityControlGrid" task
    When I complete "ReviewOnboarding" task
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    When I navigate to "FLODKYCReviewandSign-OffGrid" task
    When I complete "FLODKYCReviewandSign-Off" task
    When I navigate to "FLODAVPReviewandSign-OffGrid" task
    When I complete "FLODAVPReviewandSign-Off" task
    When I navigate to "BUHReviewandSignOffGrid" task
    When I complete "BUHReviewandSignOff" task
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFabReferences" task
    #Validating that the case status is closed
    When I navigate to "LEverifieddetails" task screen by navigating through "LE360-overview" screen
    When I navigate to "Internal Booking details" section
    #Test Data:Verify new auto-populated fields under "Internal Booking details" section on "LEverifieddetails" screen
    And I validate the following new fields under "Internal Booking details" section
      | Target Code                 |
      | Sector Description          |
      | UID Originating Branch      |
      | Propagate To Target Systems |
