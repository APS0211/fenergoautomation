#Test Case: TC_R1EPIC002PBI017.2_01
#PBI: R1EPIC002PBI017.2
#User Story ID:USR01
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.2_01

  Scenario: Validate the renamed fields under "Internal Booking details" section on "LE360-overview" screen for RM user
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "CaptureRequestDetailsFAB" task
    Then I navigate to "LE360-overview" task
    #Test Data:Verify renamed fields under "Internal Booking details" section
    And I validate the following renamed fields under "Customer details" section
      | Current Label        | Renamed label   |
      | Entity of Onboarding | Booking Country |
