#Test Case: TC_R1EPIC002PBI020_03
#PBI: R1EPIC002PBI020
#User Story ID: US009, US010, US068, US109, US111
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: Enrich Client Information (Capture Hierarchy Details>Associated Parties>Association Details)

@To_be_automated @TC_R1EPIC002PBI020_03

Scenario: Validate behaviour of fields in Capture Hierarchy Details>Associated Parties>Association Details screen when an already existing entity is added as associated party 

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "Corporate" and LegalEntityrole as "Client/Counterparty"  
	And I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYCandRegulatoryFAB" task 
    When I navigate to "EnrichKYCProfileGrid" task 
    When I complete "EnrichKYCProfileFAB" task 
    When I navigate to "CaptureHierarchyDetailsGrid" task 
    #Perform below step by right clicking the legal entity from hierarchy and clicking "Add Associated Party"
   	And I navigate to "Associated Parties" screen for the entity
   	#Fill data that matches an entity that is already there in the system 
	  And I fill mandatory fields
	  #Perform below step by clicking "Add Selected" button
	  And I navigate to "Association Details" screen of expressly added associated party 
	And I validate the only following fields in "Association" section
		Fenergo Label Name					|Field Type					|Visible	|Editable	|Mandatory	|Field Defaults To 	|
		Assoication Type 						|Drop-down					|Yes			|Yes			|Yes				|Select...  				|
		Type of Control             |Drop-down					|Yes			|Yes			|Yes        |Select...          |
	And I validate LOVs of "Assoication Type" field
	#Refer PBI for LOV list	
	And I validate LOVs of "Type of Control" field
	#Refer PBI for LOV list	
	And I validate "Is BOD?" field is not visible
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I validate "Shareholding %" field is not visible
	And I select "Director" for "Assoication Type" field
	And I validate "Is BOD?" field is visible
	And I validate the conditional field in "Association" section
		Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		Is BOD?           			|Drop-down		|Yes			|Yes				|Yes				|Select...					|
	And I validate LOVs of "Is BOD?" field
		|Yes|
		|No |
	And I select "Yes" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is visible
	And I validate the conditional field in "Association" section
		Fenergo Label Name							|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		Is Greater than 5% Shareholder?	|Alphanumeric	|Yes			|Yes				|Yes				|NA									|
	And I select "Has Shareholder" for "Assoication Type" field
	And I validate "Shareholding %" field is visible
	#This field should be displayed below "Assocation Type" field
	And I validate the conditional field in "Association" section
		Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		Shareholding %					|Numeric			|Yes			|Yes				|No					|NA									|