#Test Case: TC_R1EPIC002PBI020_05
#PBI: R1EPIC002PBI020
#User Story ID: US068, US109, US111
#Designed by: Anusha PS
#Last Edited by: Anusha PS

Feature: AML (Complete AML>Associated Parties>Association Details)

@To_be_automated @TC_R1EPIC002PBI020_02

Scenario: Validate behaviour of fields in Complete AML>Associated Parties>Association Details for expressly added associated party - part 2

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with ClientEntityType as "Corporate" and LegalEntityrole as "Client/Counterparty"  
	And I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
    When I navigate to "ValidateKYCandRegulatoryGrid" task 
    When I complete "ValidateKYCandRegulatoryFAB" task 
    When I navigate to "EnrichKYCProfileGrid" task 
    When I complete "EnrichKYCProfileFAB" task 
    When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
    #Perform below step by right clicking the legal entity from hierarchy and clicking "Add Associated Party"
   	And I navigate to "Associated Parties" screen for the entity 
	  And I fill mandatory fields
	  #Perform below step by clicking "Create New LE" button
	  And I navigate to "Association Details" screen of expressly added associated party 
	And I validate "Is BOD?" field is not visible
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I validate "Shareholding %" field is not visible
	And I select "Non Executive Director" for "Assoication Type" field
	And I validate "Is BOD?" field is visible
	And I validate the conditional field in "Association" section
		Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		Is BOD?           			|Drop-down		|Yes			|Yes				|Yes				|Select...					|
	And I select "No" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I select "CEO" for "Assoication Type" field
	And I validate "Is BOD?" field is not visible
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I select "Trustee" for "Assoication Type" field
	And I validate "Is BOD?" field is visible
	And I select "Yes" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is visible
	And I select "Ultimate Beneficial Owner (UBO)" for "Assoication Type" field
	And I validate "Is BOD?" field is visible and dropdown should be defaulted "Select..."
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I select "No" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I select "Yes" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	#Play around with different combination like above steps
	
	