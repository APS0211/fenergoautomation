#Test Case: TC_R1EPIC002PBI013_03
#PBI: R1EPIC002PBI013
#User Story ID: US122
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB 

@Automation
Scenario: Validate the behavior of "DAO Code" drop-down field on "User management" screen for "Compliance" user
	Given I login to Fenergo Application with "Admin"
	When I navigate to "UserManagement" link by clicking on "Security" link under options
	
	# Search for Compliance(user3) on user management screen and navigate to users screen
	When I search for "user3" user by providing value in "Firstname" textbox.
	Then I can see "DAO Code" drop-down is displaying under "Userdetails" section
	