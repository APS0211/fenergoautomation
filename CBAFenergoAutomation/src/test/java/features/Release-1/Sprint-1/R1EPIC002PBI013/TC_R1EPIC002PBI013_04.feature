#Test Case: TC_R1EPIC002PBI013_04
#PBI: R1EPIC002PBI013
#User Story ID: US122
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: COB

@Automation @TC_R1EPIC002PBI013_04
Scenario: Validate the LOVs for "DAO Code" drop-down field on "User management" screen
	Given I login to Fenergo Application with "Admin"
	When I navigate to "UserManagement" link by clicking on "Security" link under options
    # Search for Relationship Manager(user1) on user management screen and navigate to users screen
	When I search for "user1" user by providing value in "Firstname" textbox.
	# Refer latest PBI for "DAOCode" LOVs
	Then I verify "DAO Code" drop-down values
	