#Test Case: TC_R1EPIC002PBI009_05
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

@Automation @TC_R1EPIC002PBI009_05
Scenario: Verify the new field 'DAO code' is available under User Details section of Add Relationship screen when the user is associated to RM group
	
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I search user from "Relationship" on "CaptureRequestDetails"
	#Select relationship which is associated to RM group
	#check the DAO Code value which is linked to the RM user
    Then I can see "DAO Code" drop-down is displaying under "Userdetails" section


	