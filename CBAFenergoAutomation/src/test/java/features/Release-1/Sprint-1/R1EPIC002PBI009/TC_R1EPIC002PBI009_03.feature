#Test Case: TC_R1EPIC002PBI009 _03
#PBI: R1EPIC002PBI009
#User Story ID: US055
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: Capture New Request Details/Review Request - Add Relationship -Relationship Type

@Automation @TC_R1EPIC002PBI009_03
Scenario: Verify the ability to search relationship using 'DAO code' in Search Users section of Add Relationship screen in Capture request details creen
	
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	#Searching relationships using DAO Code
	#Enter valid DAO Code Ex: BNK103330737
	When I search user from "Relationship" on "CaptureRequestDetails"
#	When I click on Search button in AddRelationship screen
#	#User should be able to view the relationships in the search grid corresponding to the DAO Code entered in the search criteria
#	Then I validate the search grid data
#	