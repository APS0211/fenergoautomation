#Test Case: TC_R1EPIC002PBI004_02
#PBI: R1EPIC002PBI004
#User Story ID: US068, US024, US025, US087, US026, US040, US039, US033, US041
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: Capture New Request Details - Customer Details

  @TobeAutomated
  Scenario: Verify the conditionally available field behaviour of new fields added in Customer Details section of Capture Request Details Screen
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = Andora (other than UAE) and client type = PCG Entity
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I check that below data is available
      | FieldLabel                                                 | Field Type | Visible | Mandatory | Editable | Field Defaults to | Visible |
      | Channel and Interface                                      | Dropdown   | Yes     | Yes       | Yes      | Select...         |         |
      | Justification for opening/maintaining non-resident account | Textbox    | Yes     | Yes       | Yes      |                   |         |
      | Customer Tier                                              | Dropdown   | Yes     | No        | Yes      | Select...         |         |
      | Residential Status                                         | Dropdown   | Yes     | Yes       | No       |                   |         |
      | Emirate                                                    | Dropdown   |         |           |          |                   | No      |
    #Verify Residential status is set to Resident when country of Incorporation = Andora (other than UAE)
    And I check that below data is available
      | FieldLabel         | Value        |
      | Residential Status | Non Resident |
