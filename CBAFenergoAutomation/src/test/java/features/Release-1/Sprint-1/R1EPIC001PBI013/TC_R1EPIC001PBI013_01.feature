#Test Case: TC_R1EPIC001PBI013_01
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LE360-overview screen-"Anticipated Activity of Account" field

Scenario: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "LE360-overview" stage for RM user.
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 
	When I navigate to "LE360overview" task
	#Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible