#Test Case: TC_R1EPIC001PBI013_02
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Enrich KYC Profile screen-"Anticipated Activity of Account" field

@To be automated 

Scenario: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "Enrich client Profile screen" stage for onboarding maker.
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete to "ReviewRequest" task	
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task	
	When I complete "ValidateKYCandRegulatoryGrid" task	
	When I navigate to "EnrichKYCProfileGrid" task
	#Test Data:Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
	Then I can see "Anticipated Activity of Account" field is not visible