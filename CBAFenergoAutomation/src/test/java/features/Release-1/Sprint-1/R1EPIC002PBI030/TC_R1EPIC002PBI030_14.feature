#Test Case: TC_R1EPIC002PBI030_14
#PBI: R1EPIC002PBI030
#User Story ID: US084
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: "Enrich client profile"- "Source Of Funds And Wealth Details" section

  Scenario: Validate for conditional field "If Yes, Specify the Sanctioned Countries/Territories" under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty" and client Type "FI" or "NBFI"
    When I navigate to "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I set "Active Presence in Sanctioned Countries/Territories" field as "yes"
    # Test data- Validate conditional field "If Yes, Specify the Sanctioned Countries/Territories" is visible else hidden under "Source Of Funds And Wealth Details" section
    Then I validate "If Yes, Specify the Sanctioned Countries/Territories" field is visible
