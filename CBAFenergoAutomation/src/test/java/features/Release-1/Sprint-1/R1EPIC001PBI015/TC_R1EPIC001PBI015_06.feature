#Test Case: TC_R1EPIC001PBI015_06
#PBI: R1EPIC001PBI015
#User Story ID: TOM156
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI015_06

Scenario: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid 
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingChecker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I navigate "RelationshipManagerReviewandSign-Off"Grid task 
  Then I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
	
	#Test data: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid on "Case details" screen
	Then I can see case status for case is displaying as "Referred for Reprocessing" in Tasks grid
	
Scenario: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid for "Risk Assessment" Stage for "FLOD KYC manager"
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
   When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingChecker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I navigate to "RelationshipManagerReviewandSign-Off"Grid task
	When I complete "RelationshipManagerReviewandSign-Off"Grid task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODKYCManager" 
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
  Then I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
	
	#Test data: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid on "Case details" screen
	Then I can see case status for case is displaying as "Referred for Reprocessing" in Tasks grid
	
	
Scenario: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid for "Risk Assessment" Stage for "FLOD AVP"
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingChecker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I navigate to "RelationshipManagerReviewandSign-Off"Grid task
	When I complete "RelationshipManagerReviewandSign-Off"Grid task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODKYCManager" 
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODAVP" 
	When I search for "CaseID"
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
  Then I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
	
	#Test data: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid on "Case details" screen
	Then I can see case status for case is displaying as "Referred for Reprocessing" in Tasks grid
	
	
Scenario: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid for "Risk Assessment" Stage for "Business Unit Head (N3)"
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
   When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingChecker" 
	When I search for "CaseID"
	When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I navigate to "RelationshipManagerReviewandSign-Off"Grid task
	When I complete "RelationshipManagerReviewandSign-Off"Grid task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODKYCManager" 
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODAVP" 
	When I search for "CaseID"
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
  When I complete "FLODAVPReviewandSign-Off" task
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "BusinessUnitHead(N3)" 
	When I search for "CaseID"
	When I navigate to "BUHReviewandSignOffGrid" task 
	Then I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
	
	#Test data: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid on "Case details" screen
	Then I can see case status for case is displaying as "Referred for Reprocessing" in Tasks grid
