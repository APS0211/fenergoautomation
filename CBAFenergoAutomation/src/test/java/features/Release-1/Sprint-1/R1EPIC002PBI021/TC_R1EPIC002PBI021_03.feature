#Test Case: TC_R1EPIC002PBI021_03
#PBI: R1EPIC002PBI021
#User Story ID: US017/18, US123, US125
#Designed by: Anusha PS
#Last Edited by: Anusha PS

@To_be_automated

Feature: Validate KYC and Regulatory Data> Address Section  

Scenario: Validate "Address" subflow in "Validate KYC and Regulatory Data" screen is non-mandatory and has all the fields / behavior as per DD (Label Name/Sequence/Field Type/Visible/Editable/Mandatory/Field Defaults To)					

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "FI" and "Legal Entity Role" as "Client/Counterparty "
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
  When I complete "ValidateKYCandRegulatoryFAB" task as "Onboarding Maker"
	When I navigate to "EnrichKYCProfileGrid" task 
	
	And I assert that "Address" subflow is mondatory
	#Navigate to Address Sub flow by clicking the "+" button
	And I validate the following fields in "Address" Sub Flow
		Fenergo Label Name			|Label Sequence	|Field Type							|Visible	|Editable	|Mandatory	|Field Defaults To 	|
		ID											|1							|Alphanumeric						|Yes			|No				|No					|NA									|
		Address Type						|2							|Drop-down							|Yes			|Yes			|Yes				|Select...					|
		Address Line 1 					|3							|Alphanumeric						|Yes			|Yes			|Yes				|NA                	|
		Address Line 2					|4							|Alphanumeric						|Yes			|Yes			|No					|NA                	|
		Town / City							|5							|Alphanumeric      			|Yes			|Yes			|No					|NA               	|
		ZIP Code 								|6							|Alphanumeric      			|Yes			|Yes			|No					|NA               	|
		PO Box  								|7							|Alphanumeric         	|Yes			|Yes			|Conditional|NA             	  |
		Country  								|8							|Drop-down            	|Yes			|Yes			|No					|Select...         	|
		State / Emirate 				|9							|Drop-down							|Yes			|Yes			|No					|Select...         	|
		
	And I select "United Arab Emirates" for Country field
	And I check that "PO Box" is Mandatory
	And I check that "State / Emirate" field is Mandatory
	And I assert that "State / Emirate" field is editable
	And I validate LOVs of "State / Emirate" field 
		|Abu Dhabi      |
		|Ajman					|
		|Dubai					|
		|Fujairah				|
		|Ras Al Khaimah	|
		|Sharjah				|
		|Umm Al Quwain	|
		
	And I select "India" for Country field
	And I check that "PO Box" is non-Mandatory
	And I assert that "State / Emirate" field is greyed-out	
	
	And I add "correspondance" address