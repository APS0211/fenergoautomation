#Test Case: TC_R1EPIC001PBI014_01
#PBI: R1EPIC001PBI014
#User Story ID: US024a
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI014_01

Scenario: Validate "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen  when case is referred back to previous stage
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
	
  When I navigate to "EnrichKYCProfileGrid" task
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task
	When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  When I store the "CaseId" from LE360 
  When I login to Fenergo Application with "OnboardingMaker" 
  When I search for "CaseID"
  When I navigate to "QualityControlGrid" task 
	When I complete "QualityControlGrid" task 
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  When I store the "CaseId" from LE360 
	When I login to Fenergo Application with "RM" 
	When I search for "CaseID"
	When I complete "RelationshipManagerReviewandSign-Off"Grid task
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODKYCManager" 
	When I search for "CaseID"
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "FLODAVP" 
	When I search for "CaseID"
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  
  Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "BusinessUnitHead(N3)" 
	When I search for "CaseID"
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
  When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
  Then I can see "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen 
   
  Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "CreateFABrecord" task
	When I Complete to "CreateFABrecord" task
	When I navigate to "casedetails" screen
  When I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
  When I navigate to "Case Referral" screen 
  When I select "RiskAssessment" task from "RefertoStage" drop-down
  #Test data- verify "Referral Reason" Text-box is displaying as mandatory on "Case Referral" screen  and case is referred to "RiskAssessment" stage 
  Then I can see "ReferralReason" Text-box is displaying as mandatory on "Case Referral" screen 
   
  When I enter value in "ReferralReason" Text-box and click on "Refer" button
  Then I can see that Case has been referred to "RiskAssessment" stage 
  
  
  
  
  
  
  
   
   
	
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
