Feature: TC_R1EPIC001PBI006 _06

Scenario: Verify the  FLOD AVP user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "FLODAVPUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any FLOD AVP user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the FLOD AVP user
	Then I validate the search grid data
