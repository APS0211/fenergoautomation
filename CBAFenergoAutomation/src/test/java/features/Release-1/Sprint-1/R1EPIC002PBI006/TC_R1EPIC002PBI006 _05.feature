#Test Case: TC_R1EPIC002PBI006_05
#PBI: R1EPIC002PBI006
#User Story ID: US139
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_05
  Scenario: Verify the 'Is UAE Licensed' field value is set to False when Country of Incorporation is other than UAE and verify the Countries of Business Operations/Economic Activity' field is non mandatory if client type is other than corporate
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = Andora and client type = FI
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                                         | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Is UAE Licensed                                    | Auto       | No        | Yes      | False             | Yes     |
      | Countries of Business Operations/Economic Activity | Dropdown   | No        | Yes      | Select...         | Yes     |
