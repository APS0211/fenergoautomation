#Test Case: TC_R1EPIC002PBI007_01
#PBI: R1EPIC002PBI007
#User Story ID: US020, US021, US022, US023, US0126, US0127
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC002PBI007_01 

@Automation @TC_R1EPIC002PBI007_01 
Scenario: 
	Validate behavior of fields in Capture New Request Details - Contacts section 
	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I navigate to "Contacts" screen by clicking on "Plus" button from "CaptureRequestDetails" 
	And I validate the following fields in "Contacts" Sub Flow 
		|  Label               | FieldType    | Visible | Editable    | Mandatory   | DefaultsTo        |
		| Primary Phone Number | Dropdown     | true     | false      | true        | Work phone        |
		| Home Phone           | Numeric      | true     | false      | false       | NA                |
		| Work Phone           | Numeric      | true     | false      | true        | NA                |
		| Mobile               | Numeric      | true     | false      | false       | NA                |
		| Fax                  | Alphanumeric | true     | false      | false       | NA                |
		| Email Address        | Alphanumeric | true     | false      | true        | NA                |
		| Is Primary Contact   | CheckBox    |  true     | false      | false       | NA                |
		And I fill the data for "Contacts" with key "A1"
		
		And I assert that "EmailAddress" accepts less than or equal to "255" characters 
        And I assert that "EmailAddress" does not accept more than 255 characters 
		And I assert that "WorkPhone" accept less than or equal to "14" characters 
		And I assert that "WorkPhone" does not accept more than "14" numbers 
		And I assert that "WorkPhone" does not accept alphanumerals 
		And I assert that "HomePhone" accepts less than or equal to "14" numbers
		And I assert that "HomePhone" does not accept more than "14" numbers 
		And I assert that "HomePhone" does not accept alphanumerals 
		And I assert that "Mobile" accepts less than or equal to "14" numbers
		And I assert that "Mobile" does not accept more than "14" numbers
		And I assert that "Mobile" does not accept alphanumerals
		 
	
		