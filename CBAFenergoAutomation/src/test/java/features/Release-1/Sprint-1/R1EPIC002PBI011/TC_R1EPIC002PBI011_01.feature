#Test Case: TC_R1EPIC002PBI011_01
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: Enrich KYC profile task -Customer Details - Tax Identifier 



@Automation @TC_R1EPIC002PBI011_01
Scenario: Validate the behavior of "Tax Value field" for TAX identifier Type as "VAT ID" and Country as "UAE" in Capture Request Details screen
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES"
	And I check that "Tax Type" is Mandatory
    And I enter "TaxIdentifierValue" as "Lessthan15character"
	Then I validate the error messgage for "TaxIdentifierValue" as "Tax value for VAT ID is not valid. Please enter numeric values of maximum 15 character length."