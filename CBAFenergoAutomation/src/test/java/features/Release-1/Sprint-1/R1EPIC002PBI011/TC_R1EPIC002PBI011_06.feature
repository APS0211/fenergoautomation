#Test Case: TC_R1EPIC002PBI011_06
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: COB 

@TC_R1EPIC002PBI011_06
Scenario: Validate the behavior of "Tax Value field" for TAX identifier Type as "VAT ID" and Country as "UAE" in Enrich KYC Profile screen
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	 And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	Then I store the "CaseId" from LE360
	And I complete "ReviewRequest" task
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	When I complete "ValidateKYCandRegulatoryFAB" task
	
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	Then I navigate to "EnrichKYCProfileGrid" task
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES"
	And I check that "Tax Type" is Mandatory
#	Then I can see "TaxIdentifierValue" is displaying as mandatory and exactly 15 numeric characters
#	Then I can see "TaxIdentifierValue" accept less than 15 numeric characters
