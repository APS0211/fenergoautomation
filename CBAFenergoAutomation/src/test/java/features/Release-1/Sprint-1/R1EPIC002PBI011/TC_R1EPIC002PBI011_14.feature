#Test Case: TC_R1EPIC002PBI011_14
#PBI: R1EPIC002PBI011
#User Story ID: US107
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Enrich KYC profile task -Identifier Sub flow - Country of Tax Residency  

@TC_R1EPIC002PBI011_14 
Scenario: Validate LOVs for "Country" field in Enrich KYC Profile screen
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	 And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	And I complete "ReviewRequest" task
	And I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	And I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I verify "Country of Incorporation / Establishment" drop-down values
	And I complete "ValidateKYC" screen with key "C1"
	And I click on "SaveandCompleteforValidateKYC" button 
#	And I complete "ValidateKYC" screen with key "C1"
	Then I navigate to "EnrichKYCProfileGrid" task
	Then I navigate to "TaxIdentifier" screen by clicking on "Plus" button from "EnrichKYCProfile"
	Then I verify "Country" drop-down values
	#Refer PBI for the list