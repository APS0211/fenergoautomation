#Test Case: TC_R1EPIC01PBI001_03
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI003_03

@automation @R1EPIC01PBI003 
Scenario: Validate if "Product Details" section is removed on Add Product Screen for "Capture request details screen"(using Edit button)
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I add a Product from "CaptureRequestDetails"
	Then I can see product is visible in Product grid
	When I navigate to "Product" screen by clicking on "Edit" button from "CaptureRequestDetails"
	Then I can see "ProductDetails" section is not visible
	
	
