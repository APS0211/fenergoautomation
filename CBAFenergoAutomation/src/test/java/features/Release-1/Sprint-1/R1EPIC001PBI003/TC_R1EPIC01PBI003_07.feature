#Test Case: TC_R1EPIC01PBI001_07
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI001_07 
@TC_R1EPIC01PBI001_07
Scenario: Validate if "Product Details" section is removed for the following users for "Enrich KYC Profile screen".
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
	|Product|Relationship|
	|C1|C1|
	And I click on "Continue" button
	And I store the "CaseId" from LE360
	When I complete "ReviewRequest" task
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	And I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button
	When I navigate to "EnrichKYCProfileGrid" task 
	Then I can see product is visible in Product grid for "EnrichKYCProfile"
	Then I can see "ProductDetails" section is not visible
	When I complete "AddAddress" task
	And I complete "EnrichKYC" screen with key "C1" 
	Given I login to Fenergo Application with "FLoydKYC"
	When I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see product is visible in Product grid for "ValidateKYCRequest"
	When I view the existing Product from "EnrichKYCProfile"
	Then I can see "ProductDetails" section is not visible 
#	When I login to fenergo Appication with "FLODKYCManager"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "FLODAVP"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "BusinessUnitHead(N3)"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "FLODVP"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "FLODSVP"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "BusinessHead(N2)"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	When I login to Fenergo Application with "Compliance"
#	When I search for the "CaseId" 
#	When I navigate to product on "CaptureRequestDetailsGrid" task by clicking on "view" button
#	When I navigate to "Add Product" screen again
#	Then I can see "Product details" section is not visible on Add Product screen
#	
#	
#	
#	