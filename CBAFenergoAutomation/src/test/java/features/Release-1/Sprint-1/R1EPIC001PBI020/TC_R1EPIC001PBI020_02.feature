#Test Case: TC_R1EPIC001PBI020_02
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Review Request screen-"Telephone number" field

Scenario: Verify value of field "Telephone number" is auto-populated on "Review Request" task of "New Request" stage for onboarding maker.
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	Then I enter exact 14 numbers in "Telephone number" field in format "CCCNNNNNNNNNNN"
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete to "ReviewRequest" task	
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task	
	When I complete "ValidateKYCandRegulatoryGrid" task	
	When I navigate to "EnrichKYCProfileGrid" task
	#Test Data: Auto-populated value in "Telephone number" field in format "CCCNNNNNNNNNNN exact 14 numbers"  under "GLCMS" section 
	Then I can see "Telephone number" field is auto-populated with the value in format "CCCNNNNNNNNNNN exact 14 numbers" under "GLCMS" section
	And I save the details successfully