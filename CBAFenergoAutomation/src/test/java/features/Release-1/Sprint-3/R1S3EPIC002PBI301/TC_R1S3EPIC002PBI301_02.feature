#Test Case: TC_R1S3EPIC002PBI301_02
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF048, OOTBF049
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
@Tobeautomated
Feature: TC_R1S3EPIC002PBI301_02

  Scenario: Verify "Business Markets" section is displaying as hidden on "Enrich KYC Profile" task of "Enrich Client Profile" Stage for KYC Maker.
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Test-data: Verify "Business details" section is displaying as hidden under "Customer details" section
    Then I see "Businessdetails" section is displaying as hidden under "Customer details" section
