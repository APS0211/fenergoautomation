#Test Case: TC_R1S3EPIC002PBI301_03
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF050
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI301_03

  Scenario: Validate the address screen before and after implementation (No changes)
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    Then I navigate to "Address" task to add address
    #Test-data: Address screen to come up with no changes.Displaying same as earlier (refer snaphot from designer)
    And I validate that address screen is displaying as earlier
