#Test Case: TC_R1S3EPIC002PBI301_01
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
@Tobeautomated
Feature: TC_R1S3EPIC002PBI301_01

  Scenario: Verify "legal Status" label is displaying as greyed out (Read-only) under"Customer details" section "Enrich Client Profile" Stage for KYC Maker.
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Test-data: Verify "legal Status" label is displaying as greyed out (Read-only) under "Customer details" section
    Then I see "LegalStatus" label is displaying as greyed out under "Customer details" section
