#Test Case: TC_R1S3EPIC002PBI300_02
#PBI: R1S3EPIC002PBI300
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI300_02

  Scenario: "Add Details" button is displaying as hidden on "Product" screen of  "Capture request details" task of "New request" Stage for RM & Onboarding / KYC Maker.
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I navigate to "Product" task by clicking on + sign displaying at the top of "product" grid
    #Test-data:  "Add Details" button is displaying as hidden on "Product" screen
    Then I verify "Add Details" button is displaying as hidden on "Product" screen
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I navigate to "products" task by clicking on Edit button
    #Test-data:  "Add Details" button is displaying as hidden on "Product" screen
    Then I verify "AddDetails" button is displaying as hidden on "Product" screen
