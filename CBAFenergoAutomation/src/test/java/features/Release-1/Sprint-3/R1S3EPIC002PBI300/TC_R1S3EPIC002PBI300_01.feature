#Test Case: TC_R1S3EPIC002PBI300_01
#PBI: R1S3EPIC002PBI300
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI300_01

  Scenario: "Trading Entity" grid is displaying as greyed out ('+' sign dislaying at the top of the grid is disabled ) on "Capture request details" task of "New request" Stage for RM & Onboarding / KYC Maker.
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    #Test-data: "Trading Entity" grid is displaying as greyed out ('+' sign dislaying at the top of the grid is disabled )
    Then I verify  "TradingEntity" grid is displaying as greyed out and expanded
    And + sign displaying at the Top of "TradingEntity" grid is disabled
