package parallel;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import dataProvider.ConfigFileReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import cucumber.api.java.After;

public class Login extends TestRunner {

	public WebDriver driver;
	public static JavascriptExecutor jsExecutor;
	public static String AuthToken;
	JavaScriptControls js;
	ScenarioContext scenariocontext = new ScenarioContext();
	ConfigFileReader configFileReader = new ConfigFileReader();

	@Given("^I login to Fenergo Application with \"([^\"]*)\"$")
	public void login_with_user(String username) {

		try {
			System.out.println("Threadnumber=" + Thread.currentThread().getId());
			System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());
			driver = new ChromeDriver();
			jsExecutor = (JavascriptExecutor) driver;
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			ResultSet rst = sql.getUsernameandPassword("Users", username);
			while (rst.next()) {
				String Username = rst.getString("Username");
				String password = rst.getString("Password");
				driver.get(configFileReader.getApplicationUrl());
				String url = driver.getCurrentUrl();
				String url2 = url.replace("http://", "");

				String url3 = "http://" + Username + ":" + password + "@" + url2;

				driver.get(url3);

				driver.manage().window().maximize();
				System.out.println("Logged in Succesfully");
				Thread.sleep(7000);

			}
		} catch (Exception e) {
			System.out.println("Unable to login with user " + username);
			assertTrue(false, e.getLocalizedMessage());
			driver.close();

		}

	}

	@And("^I assert that \"([^\"]*)\" subflow is visible in \"([^\"]*)\" screen$")
	public void i_assert_that_subflow_is_visible_in__screen(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean present = driver.findElement(By.xpath(locator)).isDisplayed();
			Assert.assertEquals(true, present);

		}
	}

	@And("^I assert that \"([^\"]*)\" is non-mondatory $")
	public void i_assert_that_something_is_nonmondatory(String strArg1) throws Throwable {

	}

	@After
	public void embedScreenshot(Scenario scenario) {
		
			try {
				final byte[] scrShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(scrShot, "image/png");
//				File DestFile = new File("src/test/resources/Screenshot.png");
//				FileUtils.copyFile(SrcFile, DestFile);

			} catch (Exception e) {
				e.printStackTrace();
			

		}
	}

	@Then("^I see there is no validation message$")
	public void i_see_there_is_no_validation_message() throws Throwable {

	}

	@When("^I click on \"([^\"]*)\" sign to create new request$")
	public void i_click_on__sign_to_create_new_request(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		String query = "select * from Controls1 where ObjectKey='CompleteRequestFAB' and Screen='CaptureRequestDetails'";
		ResultSet rst = sql.GetDataSet(query);
		String locator = rst.getString("Locator");
		WebElement element = driver.findElement(By.xpath(locator));
		jsExecutor.executeScript("arguments[0].click()", element);

	}

	@Then("^I can see \"([^\"]*)\" label is renamed as \"([^\"]*)\" on EnterEntitydetails screen$")
	public void i_can_see_label_is_renamed_on_enterentitydetails_screen(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		String query = "select * from Controls1 where ObjectKey= '" + strArg2 + "'";
		String query1 = "select * from Controls1 where ObjectKey= '" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		WebElement element1 = driver.findElement(By.xpath(locator1));
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
		Boolean FabEntityTypePresent = driver.findElements(By.xpath(locator1)).size() < 1;
		Thread.sleep(3000);
		Assert.assertEquals(true, isPresent);
		Assert.assertEquals(true, FabEntityTypePresent);

	}

	@Then("^I can see \"([^\"]*)\" label is renamed as \"([^\"]*)\" on \"([^\"]*)\" screen$")
	public void i_can_see_label_is_renamed_on_enterentitydetails_screen(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		String query = "select * from Controls1 where ObjectKey= '" + strArg2 + "'";
		String query1 = "select * from Controls1 where ObjectKey= '" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
		Boolean FabEntityTypePresent = driver.findElements(By.xpath(locator1)).size() < 1;
		Thread.sleep(3000);
		Assert.assertEquals(true, isPresent);
		Assert.assertEquals(true, FabEntityTypePresent);

	}

	@And("^I assert that \"([^\"]*)\" accepts less than or equal to \"([^\"]*)\" characters$")
	public void i_assert_that_something_accepts_less_than_or_equal_to_255_characters(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" accepts less than or equal to \"([^\"]*)\" numbers$")
	public void i_assert_that_something_accepts_less_than_or_equal_to_255_numbers(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" accept less than or equal to \"([^\"]*)\" characters$")
	public void i_assert_that_something_accept_less_than_or_equal_to_255_characters(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" does not accept alphanumerals$")
	public void i_assert_that_something_does_not_accept_alphanumerals(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(23));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(13));
		}
	}

	@And("^I assert that \"([^\"]*)\" does not accept more than \"([^\"]*)\" numbers$")
	public void i_assert_that_something_does_not_accept_more_than_numbers(String strArg1, String strArg2)
			throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(15));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Disabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(14));
		}
	}

	@And("^I assert that \"([^\"]*)\" does not accept more than 255 characters$")
	public void i_assert_that_something_does_not_accept_more_than_255_characters(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(257));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Disabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(255));
		}

	}

	public void FillInDataWithLocators(String type, String xpath, String value, String labelName) {

		try {
			switch (type) {

			case "Textbox":
				TextBox.fillInLegacyPageTextBox(xpath, value);
				break;
			case "TextArea":
				TextArea.fillInLegacyPageTextArea(xpath, value);

			}

		} catch (Exception e) {

		}

	}

	@And("^I assert that the CaseStatus is \"([^\"]*)\"$")
	public void i_assert_that_the_casestatus_is_closed(String strArg1) throws Throwable {
//		driver.navigate().refresh();
		Thread.sleep(10000);
		WebElement casestatus = driver.findElement(By.xpath("//div[@class='fen-form-field-wrapper']"));
		casestatus.getText();
		System.out.println("Case status is " + casestatus);

		if (casestatus.getText().equals("Closed")) {
			System.out.println("Case status is Closed");

		} else {
			System.out.println("Match Not found");
			Assert.assertEquals("Closed", casestatus.getText());
//		driver.close();
		}
		driver.close();
	}

	public void FillInDataWithoutValidation(ResultSet dataSet) {

		try {

			while (dataSet.next()) {
//				String value = dataSet.getString("FieldValue");
//				String labelName = dataSet.getString("Label");
//				String fieldName = dataSet.getString("FieldName");
//				String identificationType = dataSet.getString("IdentificationType");
//				String locator = dataSet.getString("locator");
////				String type = dataSet.getString("type");
//				if (value.contains("{1}")) {
//					value = value.replace("{1}", "_");
//					scenariocontext.setValue(fieldName, value);
//					if (identificationType != null && identificationType.equals("XPATH")) {
//						FillInDataWithLocators(type, locator, value, labelName);
//						switch (dataSet.getString("type")) {
//
//						case "TextBox":
//							driver.findElement(By.xpath(locator)).sendKeys(value);
//						case "TextArea":
//							TextArea.fillInTextArea(labelName, value, fieldName);
//						case "Dropdown":
//							Dropdown.SelectElementByText(labelName, value, fieldName);
//						case "CheckBox":
//							CheckBox.FillInCheckBox(labelName, Boolean.valueOf(value));

				String name = dataSet.getString("label");
				String value = dataSet.getString("FieldValue");
				String Type = dataSet.getString("type");
				String locator = dataSet.getString("locator");
				switch (Type) {

				case "button":
					Thread.sleep(5000);
					// System.out.println("Name is" + name);
					if (name.equals("Continue")) {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(10000);
					} else
						Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
					break;
				case "Checkbox":
					Thread.sleep(5000);
					// System.out.println("Name is" + name);

					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
				case "Radiobutton":
					Thread.sleep(5000);
					// System.out.println("Name is" + name);

					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

					break;
				case "Dropdown":
					if (value != null) {
						Thread.sleep(2000);

						if (name.equals("Can the corporation issue bearer shares?")) {
							jsExecutor.executeScript(
									"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
							Thread.sleep(5000);
						}

						else if (value.equals("NULL")) {
							break;

						}

						else
							System.out.println(jsExecutor.executeScript("PageObjects.find({caption:'" + name
									+ "'}).setValueByLookupText('" + value + "')"));
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(2000);

					}
					break;

				case "MultiSelectDropdown":
					if (value != null) {
						System.out.println(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						Thread.sleep(2000);
					}
					break;
				case "TextBox":
					if (value != null) {
						jsExecutor.executeScript("return PageObjects.find({caption:'" + name + "'}).clear()");

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);
					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
						break;
					}

				}

			}

		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}
	}

	@Then("^I complete \"([^\"]*)\" screen with key \"([^\"]*)\"$")
	@And("^I fill the data for \"([^\"]*)\" with key \"([^\"]*)\"$")
	public void user_fill_in_datakey(String tablename, String Datakey) {
		try {
			SqliteConnection sql = SqliteConnection.getInstance();
			ResultSet rst = sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);

		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}
	}

	@Then("^I validate the specific LOVs for \"([^\"]*)\"$")
	public void i_validate_the_specific_lovs_for_something(String strArg1) throws Throwable {

	}

	@Test
	@When("^I create new request with LegalEntityrole as \"([^\"]*)\"$")
	public void i_create_new_request_with_customerid(String strArg1) throws Throwable {

		try {

			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();

			ResultSet rst = sql.getControlsData("Controls1", "CompleteRequestFAB");

			while (rst.next()) {
				String name = rst.getString("Name");
				String value = rst.getString("Value");
				String Type = rst.getString("Type");
				String locator = rst.getString("Locator");

				switch (Type) {
				case "button":
					if (name.equals("NewRequestButton")) {
						Thread.sleep(2000);
						WebElement element = driver.findElement(By.xpath(locator));
						Actions actions = new Actions(driver);
						actions.moveToElement(element).click().build().perform();
						Thread.sleep(3000);

					} else if ((name.equals("Create Entity"))) {
						WebElement element = driver.findElement(By.xpath(locator));
						Actions actions = new Actions(driver);
						actions.moveToElement(element).click().build().perform();
						Thread.sleep(15000);

					} else {

						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);

						break;
					}
				case "Dropdown":
					if (name.equals("Legal Entity Role")) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");

					}

					else if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					}
					Thread.sleep(2000);
					break;
				case "TextBox":
					if (value != null) {

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);

					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
					}

				case "MultiSelectDropdown":
					if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
						Thread.sleep(2000);
					}

				}
			}
		}

		catch (Exception e) {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("Fenergo\\src\\test\\resources\\Screenshot.png"));
			driver.close();
			assertTrue(false, e.getLocalizedMessage());
			Thread.sleep(2000);
		}
	}

	@When("^I navigate to \"([^\"]*)\" screen$")
	public void i_navigate_to_something_screen(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("RiskAssessment")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
					driver.close();

				} else {
					Thread.sleep(5000);
					System.out.println("name is " + name);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
				}
				break;

			}

		}
	}

	@Then("^I can see \"([^\"]*)\" subflow is hidden$")
	public void i_can_see__subflow_is_hidden(String strArg1) throws Throwable {

		Thread.sleep(3000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
//			ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String query = "select * from Controls1 where ObjectKey='LE360' and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		String locator = rst.getString("Locator");
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 1;
		Assert.assertEquals(true, isPresent);

	}

	@When("^I complete \"([^\"]*)\" task for MaintenanceRequest$")
	public void i_complete_document_requirement_task_for_maintenancerequest(String strArg1) throws Throwable {

		try {
			Boolean isPresent = driver.findElements(By.xpath("//div[text()='Cases']")).size() > 0;
			if (isPresent == true) {
				WebElement element = driver.findElement(By.xpath("//div[text()='Cases']"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				WebElement element2 = driver.findElement(By.xpath("//a[@title='Capture Request Details']"));
				Thread.sleep(3000);
				jsExecutor.executeScript("arguments[0].click()", element2);
				Thread.sleep(2000);
			}
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			ResultSet rst = sql.getControlsData("Controls1", strArg1);
			while (rst.next()) {
				String name = rst.getString("Name");
				String value = rst.getString("Value");
				String Type = rst.getString("Type");
				String locator = rst.getString("Locator");
				switch (Type) {
				case "button":
					if (name.equals("RiskAssessment")) {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
						driver.close();

					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(2000);
					}
					break;
				case "Dropdown":
					if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(2000);
					}
					break;

				case "MultiSelectDropdown":
					if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						Thread.sleep(2000);
					}
					break;
				case "TextBox":
					if (value != null) {

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);
					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
						break;
					}

				default:

				}

			}
		} catch (Exception e) {

			driver.close();
		}
		driver.close();

	}

	@And("^I check that below data is mandatory$")
	public void i_check_that_below_data_is_mandatory(DataTable table) throws Throwable {
		String labelname;
		String newvalue;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");

			System.out.println("Value of data field is" + labelname);
			System.out.println(
					"jsExecutor.executeScript(\"return PageObjects.find({caption:'\" + data + \"'}).mandatory\")");

			String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + labelname + "'}).mandatory")
					.toString();
			System.out.println("Value of a is " + a);
			if (a.equals("true")) {
				System.out.println("Value of mandatory is" + a);
				assertTrue(true, "value is not mandatory");
				Thread.sleep(2000);
			}
		}

	}

	@When("^I enter data in \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_enter_data_in_something_as_something(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			if (strArg2.equals("ALPHABETICAL") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("A");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("ALPHABETICAL") && (name.equals("T24 CIF ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("A");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("NumericalMoreThenSix") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("12312312");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("NumericalLessThenSix") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("1231231");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("AllZeroes") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("000000");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else {
				driver.findElement(By.xpath(locator)).sendKeys(value);
			}

		}
	}

	@Then("^I validate the error messgage for \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_validate_the_error_messgage_for_something_as_something(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ErrorMessage'and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(5000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String locator = rst.getString("Locator");
			Thread.sleep(5000);
			WebElement casestatus = driver.findElement(By.xpath(locator));
			String value = casestatus.getText();
			System.out.println("label name is " + casestatus);
//			Assert.assertEquals(casestatus, strArg2);
			Assert.assertTrue(strArg2, value.contains(strArg2));

		}

	}

	@Then("^I refresh the page$")
	public void i_refresh_the_page() throws Throwable {
		driver.navigate().refresh();
		Thread.sleep(5000);
	}

	@And("^I check that below data is non mandatory$")
	public void i_check_that_below_data_is_non_mandatory(DataTable table) throws Throwable {
		String labelname;
		
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			Thread.sleep(3000);

			System.out.println("Value of data field is" +labelname);
//			System.out.println(
//					"jsExecutor.executeScript("return PageObjects.find({caption:'" + labelname + "'}).mandatory")");
			       
			String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + labelname + "'}).mandatory").toString();
			System.out.println("Value of a is " + a);
			Assert.assertEquals("false", a);
		}

	}

	@And("^I assert that \"([^\"]*)\" subflow is read only$")
	public void i_assert_that_something_subflow_is_read_only(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean ispresent = driver.findElements(By.xpath(locator)).size() > 0;
			Assert.assertEquals(false, ispresent);
		}
	}

	@When("^I expand the \"([^\"]*)\" button$")
	public void i_expand_the_button(String strArg1) throws Throwable {
		Thread.sleep(3000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			WebElement element = driver.findElement(By.xpath(locator));
			jsExecutor.executeScript("arguments[0].click()", element);
			Thread.sleep(3000);

		}

	}

	@When("^I complete \"([^\"]*)\" with TaxType as \"([^\"]*)\" and Country as \"([^\"]*)\"$")
	public void i_complete_something_with_taxtype_as_something_and_country_as_something(String strArg1, String strArg2,
			String strArg3) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		Thread.sleep(3000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}

				else {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}
				break;
			case "Dropdown":
				if (name.equals("Country")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					Thread.sleep(5000);
				}

				else if ((name.equals("Tax Type"))) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");

				} else {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");

				}
				break;

			}

		}
	}

	@When("^I complete \"([^\"]*)\" with TaxType as \"([^\"]*)\" and Country as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void i_complete_something_with_taxtype_as__and_country_as(String strArg1, String strArg2, String strArg3,
			String strArg4) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "' and Screen='" + strArg4 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(3000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}

				else {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}
				break;
			case "Dropdown":
				if (name.equals("Country")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					Thread.sleep(5000);
				}

				else if ((name.equals("Tax Type"))) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");

				} else {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");

				}
				break;

			}

		}
	}

	@Then("^I assert that the warning message appears as \"([^\"]*)\"$")
	public void i_assert_that_the_warning_message_appears_as(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "TaxIdentifierValidationMessage");
		String locator = rst.getString("Locator");

		WebElement casestatus = driver.findElement(By.xpath(locator));
		casestatus.getText();
		Assert.assertEquals(strArg1, casestatus.getText());

	}

	@When("^I enter \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_enter_taxidentifiervalue(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(3000);

		String locator = rst.getString("Locator");
		String Type = rst.getString("Type");
		String value = rst.getString("Value");
		switch (Type) {
		case "TextBox":
			if (value != null) {
				driver.findElement(By.xpath(locator)).sendKeys(value);
				Thread.sleep(2000);

			}

		}

		Actions act = new Actions(driver);
		act.moveByOffset(30, 30).click().build().perform();

	}

	@And("^I edit a Product with Product Status as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void i_edit_a_product_with_product_status_as_something_for_something(String strArg1, String strArg2)
			throws Throwable {
		Thread.sleep(3000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductEdit'and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);

				} else if (name.equals("ExpandButton")) {
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is" + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is" + Ispresent);

					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				} else {
					Thread.sleep(3000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}

				break;
			case "Dropdown":
				if (name.equals("Status")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					Thread.sleep(5000);
				}
				break;

			}

		}

	}

	@And("^I edit a Product with Product Status as \"([^\"]*)\"$")
	public void i_edit_a_product_with_product_status_as_something(String strArg1) throws Throwable {
		Thread.sleep(3000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "ProductEdit");
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);

				} else if (name.equals("ExpandButton")) {
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is" + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is" + Ispresent);

					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				} else {
					Thread.sleep(3000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}

				break;
			case "Dropdown":
				if (name.equals("Status")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					Thread.sleep(5000);
				}
				break;

			}

		}

	}

	@When("^I delete the Product from \"([^\"]*)\"$")
	public void i_delete_the_product(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "ProductDelete");
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("ExpandButton")) {
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is" + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is" + Ispresent);

					} else {
						Thread.sleep(2000);
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				}

				break;

			}

		}

	}

	@When("^I view the existing Product from \"([^\"]*)\"$")
	public void i_view_the_existing_product_from_something(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductView'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				Thread.sleep(2000);
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);

				break;

			}

		}

	}

	@And("^I assert ProductSection is empty in \"([^\"]*)\"$")
	public void i_assert__is_empty(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductExpand'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {

			String locator = rst.getString("Locator");

			Boolean ProductGridNotAvailable = driver.findElements(By.xpath(locator)).size() < 1;
			Assert.assertEquals(true, ProductGridNotAvailable);

		}

	}

	@And("^I close the browser$")
	public void i_close_the_browser() throws Throwable {
		driver.close();
	}

	@And("^I add a Product from \"([^\"]*)\"$")
	public void i_add_a_product_from_something(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();

		String query = "select * from Controls1 where ObjectKey='Product'and Screen='" + strArg1 + "'";

		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("RiskAssessment")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
					driver.close();

				} else {
					System.out.println("name is" + name);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}
				break;
			case "Dropdown":
				if (value != null) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					Thread.sleep(2000);
				}
				break;

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					Thread.sleep(2000);
				}
				break;
			case "TextBox":
				if (value != null) {

					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
					break;
				}
			case "Link":

				System.out.println("Name is" + name);

				if (name.equals("AddProduct")) {
					Thread.sleep(5000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

				} else {
					Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}
				break;

			default:

			}

		}
	}

	@When("^I expand \"([^\"]*)\" and zoom out so that grid gets fully visible$")
	public void i_expand_something_and_zoom_out_to_75(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'ProductExpand'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String screen = rst.getString("Screen");
			System.out.println("Screen name is" + screen);
			if (screen.equals("ReviewRequest")) {
				Thread.sleep(7000);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			} else {
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(3000);
			}
			((JavascriptExecutor) driver).executeScript("document.body.style.zoom='75%';");
		}
	}

	@And("^I expand \"([^\"]*)\" and zoom out so that grid gets fully visible for \"([^\"]*)\"$")
	public void expand(String strArg1, String strArg2) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'ProductGridExpand'and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String screen = rst.getString("Screen");
			System.out.println("Screen name is" + screen);
			if (screen.equals("ReviewRequest")) {
				Thread.sleep(5000);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			} else {
				Boolean Ispresent = driver.findElements(By.xpath(locator)).size() > 0;
				if (Ispresent == true) {
					Thread.sleep(5000);
					WebElement element = driver.findElement(By.xpath(locator));
					jsExecutor.executeScript("arguments[0].click()", element);
					Thread.sleep(3000);
				} else {
					continue;
				}
			}

		}

		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='75%';");
	}

	@And("^I assert \"([^\"]*)\" column is not present as a field below the grid for \"([^\"]*)\"$")
	public void i_assert_column_is_not_present_as_a_field_below_the_grid(String strArg1, String strArg2)
			throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query1 = "select * from Controls1 where ObjectKey= 'ProductExpand'and Name='" + strArg2 + "'";
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator1 = rst1.getString("Locator");
		Thread.sleep(5000);
		WebElement element1 = driver.findElement(By.xpath(locator1));
		jsExecutor.executeScript("arguments[0].click()", element1);
		Thread.sleep(2000);

		String locator = rst.getString("Locator");
		Thread.sleep(3000);
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 2;
		Assert.assertEquals(true, isPresent);
		Thread.sleep(5000);
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='100%';");

	}

	@And("^I assert that \"([^\"]*)\" is non-mondatory$")
	public void i_assert_that_is_not_mandatory(String strArg1) throws Throwable {
		try {
			System.out.println("PageObjects.find({name:'" + strArg1 + "'}).mandatory");
			String mandatory = null;
			mandatory = jsExecutor.executeScript("return PageObjects.find({name:'" + strArg1 + "'}).mandatory")
					.toString();
			System.out.println("Value of mandatory is" + mandatory);
			Assert.assertEquals(null, mandatory);
		} catch (NullPointerException npe) {

		}

	}
	@And("^I assert that \"([^\"]*)\" grid is mandatory$")
	public void i_assert_thatgrid_is_not_mandatory(String strArg1) throws Throwable {
		try {
			Thread.sleep(2000);
			System.out.println("PageObjects.find({gridName:'" + strArg1 + "'}).mandatory");
			String mandatory = null;
			mandatory = jsExecutor.executeScript("return PageObjects.find({gridName:'" + strArg1 + "'}).mandatory")
					.toString();
			System.out.println("Value of mandatory is" + mandatory);
			Assert.assertEquals(null, mandatory);
		} catch (NullPointerException npe) {

		}

	}
	@Then("^I can see \"([^\"]*)\" section is not visible$")
	public void i_can_see_section_is_not_visible(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 1;
		Assert.assertEquals(true, isPresent);

	}

	@When("^I navigate to product screen again by clicking on \"([^\"]*)\" button$")
	public void i_navigate_to_product_screen_again_by_clicking_on_button(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Name= 'Product' and ObjectKey='" + strArg1 + "'";
		System.out.println("select * from Controls1 where Name= 'Product' and ObjectKey='" + strArg1 + "'");
		ResultSet rst = sql.GetDataSet(query);
		
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			String Label=rst.getString("Label");
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				System.out.println("Label is" +Label);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(3000);

			}
		}
	}

	@When("^I navigate to \"([^\"]*)\" screen by clicking on \"([^\"]*)\" button for \"([^\"]*)\"$")
	public void i_navigate_to_screen_again_by_clicking_on_button(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Name= '" + strArg1 + "'and ObjectKey='" + strArg2
				+ "'and Screen='" + strArg3 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(2000);

			}
		}
	}

	@When("^I create a new request with FABEntityType as \"([^\"]*)\" and LegalEntityRole as \"([^\"]*)\"$")
	public void i_create_a_new_request_with_fabentitytype_and_legalentityrole_as_something(String strArg1,
			String strArg2) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();

		ResultSet rst = sql.getControlsData("Controls1", "CompleteRequestFAB");
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");

			switch (Type) {
			case "button":
				if (name.equals("NewRequestButton")) {
					Thread.sleep(2000);
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(3000);

				} else if ((name.equals("Create Entity"))) {
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(15000);

				} else {
					Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

					break;
				}
			case "Dropdown":
				if (name.equals("Legal Entity Role")) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");

				} else if (name.equals("Client Type")) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
				}

				else if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
				}
				Thread.sleep(2000);
				break;
			case "TextBox":
				if (value != null) {

					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(3000);

				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
				}

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
					Thread.sleep(2000);
				}

			}
		}
	}

	@When("^I navigate to \"([^\"]*)\" screen on \"([^\"]*)\" task$")
	public void i_navigate_to_addproduct_screen_on_something_task(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= '" + strArg2 + "' and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Thread.sleep(3000);
//		WebElement element = driver.findElement(By.xpath(locator));
//		jsExecutor.executeScript("arguments[0].click()", element);
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
		}
	}

	@When("^I search user from \"([^\"]*)\" on \"([^\"]*)\"$")
	public void i_search_user_from_something_based(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'RelationshipSearch' and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			String name=rst.getString("Name");
			String value=rst.getString("Value");
			Thread.sleep(3000);
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
				break;
			case "Dropdown":
				 if (value != null) {
					 Thread.sleep(2000);
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					}
					Thread.sleep(2000);
			}

		}
	}

	@And("^I check that the \"([^\"]*)\" button is disabled$")
	public void i_check_that_the_something_button_is_disabled(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		WebElement element = driver.findElement(By.xpath(locator));
		Boolean disabled = element.isEnabled();
		Assert.assertEquals(false, disabled);

	}

	@And("^I assert \"([^\"]*)\" column is visible in the Product Grid for \"([^\"]*)\"$")
	public void i_assert_column_is_visible_in_the_product_grid(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		System.out.println("select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'");
		ResultSet rst = sql.GetDataSet(query);

		String locator = rst.getString("Locator");
		Thread.sleep(5000);
		WebElement casestatus = driver.findElement(By.xpath(locator));

		// WebElement casestatus =
		// driver.findElement(By.xpath("(//div[@class='fen-datagrid-expandablerow__cell'])[12]"));
		casestatus.getText();
		Assert.assertEquals(strArg1, casestatus.getText());
		Thread.sleep(3000);

	}

	@Test
	@When("^I navigate to \"([^\"]*)\" task$")
	public void i_navigate_to_task(String strArg1) throws SQLException {
		try {

			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='Task'";

			ResultSet rst = sql.GetDataSet(query);
			// String name = rst.getString("Name");
			String key = rst.getString("ObjectKey");
			String locator = rst.getString("Locator");
			if (key.equals("EnrichKYCProfileGrid") || (key.equals("ValidateKYCandRegulatoryGrid"))) {
				Thread.sleep(10000);
				System.out.println("Inside Enrich KYC profile");
				Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
				// System.out.println(isPresent);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(10000);

			} else {
				Thread.sleep(5000);
				Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
				// System.out.println(isPresent);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);

				Thread.sleep(7000);
			}

		} catch (Exception e) {
			// assertTrue(false, e.getLocalizedMessage());
			// TODO Auto-generated catch block
//			driver.close();
			e.printStackTrace();

		}

	}

	@Test
	@When("^I navigate to \"([^\"]*)\" from LHN section$")
	public void i_navigate_to_from_lhn_section(String strArg1) throws Throwable {

		try {
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "Select * from Controls1 where Name='Cases'";
			ResultSet rst = sql.GetDataSet(query);
			while (rst.next()) {
				String locator = rst.getString("Locator");
				WebElement element = driver.findElement(By.xpath(locator));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(2000);

			}

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

//		Thread.sleep(3000);
	}

	@Test
	@When("^I store the \"([^\"]*)\" from LE360$")
	public void i_store_the_caseid_from_le360(String strArg1) throws Throwable {
		try {
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "select * from Controls1 where Name='CaseId'";
			ResultSet rst = sql.GetDataSet(query);
			while (rst.next()) {
				String locator = rst.getString("Locator");
				String CaseId = driver.findElement(By.xpath(locator)).getText();
				Thread.sleep(2000);
				System.out.println(CaseId);
				scenariocontext.setValue("Key", CaseId);

			}

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

	}

	@Test
	@When("^I search for the \"([^\"]*)\"$")
	public void i_search_for_the_caseid(String strArg1) throws Throwable {

		try {
			SqliteConnection sql = new SqliteConnection();

			SqliteConnection.getInstance();
			// String query="Select * from Controls1 where
			// Name='"+scenariocontext.getValue("Key")+"'";
//    	String query2="Select * from Controls1 where Name='"+strArg1+"'";

			// System.out.println(query);
//    	ResultSet rst=sql.GetDataSet(query);
			String CaseID = scenariocontext.getValue("Key").toString();

			// String CaseId=rst.getString("Value");
			driver.findElement(By.xpath("(//button[@class='site-map    '][1])")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("//a[@title='Case']")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("//input[@id='6-10']")).sendKeys(CaseID);
			Thread.sleep(4000);
			// driver.findElement(By.xpath("//a[@id='ctl00_DefaultContent_ucSmartSearch_btnSearch']")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("(//a[@class='fen-grid-link fen-grid-link'])[1]")).click();
			Thread.sleep(4000);

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

	}

	@And("^I check that below data is not visible$")
	public void i_check_that_below_data_is_not_visible(DataTable table) throws Throwable {
		String labelname;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			String value = "//label[text()='" + labelname + "']";
			System.out.println(value);
			Boolean element = driver.findElements(By.xpath(value)).size() < 1;
			Assert.assertEquals(true, element);
			Thread.sleep(2000);

		}

	}

	@And("^I check that below data is visible$")
	public void i_check_that_below_data_is_visible(DataTable table) throws Throwable {
		String labelname;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			String value = "//label[text()='" + labelname + "']";
			System.out.println(value);
			Boolean element = driver.findElements(By.xpath(value)).size() > 0;
			Assert.assertEquals(true, element);
			Thread.sleep(2000);

		}

	}

	@Then("^I select area of change as \"([^\"]*)\" and complete Maintenance request$")
	public void i_select_area_of_change_as_something_and_complete_maintenance_request(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='MaintenanceRequest'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(2000);
				break;
			case "Dropdown":
				Thread.sleep(2000);
//				System.out.println("PageObjects.find({caption:'"+ name +"'}).setValueByLookupText('"+ value +"')");
				jsExecutor.executeScript(
						"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
				break;
			case "TextBox":
				if (value != null) {
					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;

			default:
				if (name == "Create Entity Button") {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(6000);
				}
			}
		}

	}

	@Then("^I assert that \"([^\"]*)\" is not visible$")
	public void i_assert_that_something_is_not_visible(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 0;
			Assert.assertEquals(false, isPresent);

		}

	}

	@When("^I complete \"([^\"]*)\" task$")
	public void i_complete_task(String strArg1) throws Throwable {

		try {
			Boolean isPresent = driver.findElements(By.xpath("//div[text()='Cases']")).size() > 0;
			if (isPresent == true) {
				Thread.sleep(6000);
				WebElement element = driver.findElement(By.xpath("//div[text()='Cases']"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				Thread.sleep(2000);
				WebElement element2 = driver.findElement(By.xpath("//a[@title='Capture Request Details']"));
				jsExecutor.executeScript("arguments[0].click()", element2);
				Thread.sleep(2000);
			}

			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			ResultSet rst = sql.getControlsData("Controls1", strArg1);
			while (rst.next()) {
				String name = rst.getString("Name");
				String value = rst.getString("Value");
				String Type = rst.getString("Type");
				String locator = rst.getString("Locator");
				switch (Type) {

				case "button":
					Thread.sleep(5000);
					// System.out.println("Name is" + name);
					if (name.equals("Continue")) {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(10000);
					} else
						Thread.sleep(2000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
					break;
				case "Dropdown":
					if (value != null) {
						Thread.sleep(2000);

						if (name.equals("Can the corporation issue bearer shares?")) {
							jsExecutor.executeScript(
									"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
							Thread.sleep(5000);
						} else
							jsExecutor.executeScript(
									"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(2000);

					}
					break;

				case "MultiSelectDropdown":
					if (value != null) {
						System.out.println(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						Thread.sleep(2000);
					}
					break;
				case "TextBox":
					if (value != null) {

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);
					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
						break;
					}

				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			assertTrue(falsee.getLocalizedMessage(),);
//    			File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//        		FileUtils.copyFile(src,new File("C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot.png"));
//			driver.close();
		}

	}

	@And("^I click on \"([^\"]*)\" button$")
	public void i_click_on_something_button(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String key = rst.getString("ObjectKey");

			if (key.equals("SaveandCompleteforValidateKYC")) {
				Thread.sleep(3000);

				WebElement element = driver.findElement(By.xpath(locator));
				element.click();
				Thread.sleep(10000);
			}

			else {
				Thread.sleep(3000);

				WebElement element = driver.findElement(By.xpath(locator));
				element.click();
				Thread.sleep(10000);
			}
		}

	}

	@When("^I navigate to \"([^\"]*)\" link by clicking on \"([^\"]*)\" link under options$")
	public void i_navigate_to_something_link_by_clicking_on__link_under_options(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		Thread.sleep(5000);
		ResultSet rst = sql.getControlsData("Controls1", "OptionMenu");
		while (rst.next()) {
			String locator = rst.getString("Locator");

			WebElement element = driver.findElement(By.xpath(locator));
			element.click();
			Thread.sleep(2000);
		}
	}

	@When("^I navigate to \"([^\"]*)\" link by clicking on \"([^\"]*)\" link under action$")
	public void i_navigate_to_link_by_clicking_on__link_under_action(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		Thread.sleep(5000);
		String query = "select * from Controls1 where ObjectKey='" + strArg2 + "'and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");

			WebElement element = driver.findElement(By.xpath(locator));
			element.click();
			Thread.sleep(3000);
		}
	}

	@Then("^I can see \"([^\"]*)\" drop-down is displaying under \"([^\"]*)\" section$")
	public void i_can_see_something_dropdown_is_displaying_under_something_section(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String type = rst.getString("Type");
			switch (type) {

			case "Dropdown":
				String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + strArg1 + "'}).type")
						.toString();
				System.out.println("Value of type is " + a);
				Assert.assertEquals("Dropdown", a);
				break;

			}

		}
	}

	@When("^I search for \"([^\"]*)\" user by providing value in \"([^\"]*)\" textbox.$")
	public void i_search_for_something_user_by_providing_value_in_something_textbox(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "UserManagement");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String type = rst.getString("Type");
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			switch (type) {
			case "TextBox":
				if (name.equals("FirstName")) {
					driver.findElement(By.xpath(locator)).sendKeys(strArg1);
					Thread.sleep(2000);
				} else {
					driver.findElement(By.xpath(locator)).sendKeys(value);
				}

				break;
			case "button":
				if (name.equals("Edit")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}

				else {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}
				break;
			}

		}
	}

	@When("^I navigate to \"([^\"]*)\" button with ClientType as \"([^\"]*)\"$")
	public void i_navigate_to_something_button_with_clienttype_as_something(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");

			switch (Type) {
			case "button":
				if (name.equals("NewRequestButton")) {
					Thread.sleep(2000);
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(3000);

				} else if ((name.equals("Create Entity"))) {
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(15000);

				} else {

					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

					break;
				}
			case "Dropdown":

				if (name.equals("Client Type")) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
				}

				else if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
				}
				Thread.sleep(2000);
				break;
			case "TextBox":
				if (value != null) {

					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);

				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
				}

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
					Thread.sleep(2000);
				}

			}
		}
	}

	@Then("^I verify \"([^\"]*)\" drop-down values$")
	public static List<String> GetOptions(String strArg1) throws Throwable {
		List<String> ls = null;
		ArrayList<String> Actual = null;
		ArrayList<String> Expected = null;
		int num1 = 0;
		int num2 = 5;

		while (ls == null && num1 < num2) {
			try {
				Thread.sleep(10000);

				String Dropdown = (String) jsExecutor.executeScript("var perfarray=PageObjects.find({caption:'"
						+ strArg1 + "'}).lookups;" + "return JSON.stringify(perfarray);");
				// System.out.println(jsExecutor.executeScript("var
				// perfarray=PageObjects.find({caption:'" + strArg1 + "'}).lookups;"+"return
				// JSON.stringify(perfarray);"));
				CommonSteps cs = new CommonSteps();
				Actual = cs.parseListData(Dropdown, "label");
//				System.out.println(Actual);

			} catch (Exception e) {

			}
			num1++;
		}
		Expected = Expectedvalues(strArg1);
//		System.out.println("Expected is" + Expected);
//		System.out.println("Actual is" + Actual);

		Assert.assertEquals(Expected, Actual);

		return ls;

	}

	@Then("^I verify \"([^\"]*)\" drop-down values with ClientType as \"([^\"]*)\"$")
	public static List<String> GetOptions(String strArg1, String strArg2) throws Throwable {
		List<String> ls = null;
		ArrayList<String> Actual = null;
		ArrayList<String> Expected = null;
		int num1 = 0;
		int num2 = 5;

		while (ls == null && num1 < num2) {
			try {
				Thread.sleep(10000);

				String Dropdown = (String) jsExecutor.executeScript("var perfarray=PageObjects.find({caption:'"
						+ strArg1 + "'}).lookups;" + "return JSON.stringify(perfarray);");
				// System.out.println(jsExecutor.executeScript("var
				// perfarray=PageObjects.find({caption:'" + strArg1 + "'}).lookups;"+"return
				// JSON.stringify(perfarray);"));
				CommonSteps cs = new CommonSteps();
				Actual = cs.parseListData(Dropdown, "label");
//         		System.out.println("Actual is"+Actual);

			} catch (Exception e) {

			}
			num1++;
		}
		Expected = Expectedvalue(strArg1, strArg2);
		System.out.println("Expected is" + Expected);
		System.out.println("Actual is" + Actual);

		Assert.assertEquals(Expected, Actual);

		return ls;
	}

	@And("^I check that \"([^\"]*)\" is readonly$")
	public void i_check_that_something_is_readonly(String strArg1) throws Throwable {

	}

	public static ArrayList<String> Expectedvalues(String args1) throws Throwable {
		ArrayList<String> ExpectedLOV = new ArrayList<String>();
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Lovs where ObjectKey='" + args1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String value = rst.getString("Value");
			System.out.println("Value is" + value);
			ExpectedLOV.add(value);

		}
		return ExpectedLOV;

	}

	public static ArrayList<String> Expectedvalue(String args1, String args2) throws Throwable {
		ArrayList<String> ExpectedLOV = new ArrayList<String>();
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Lovs where ObjectKey='" + args1 + "' and ClientType='" + args2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String value = rst.getString("Value");
			System.out.println("Value is" + value);
			ExpectedLOV.add(value);
		}

		return ExpectedLOV;

	}

	@When("^I click on \"([^\"]*)\" from LHS$")
	public void i_click_on_something_from_lhs(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			WebElement element = driver.findElement(By.xpath(locator));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(5000);
		}

	}

	@And("^I check that \"([^\"]*)\" is Mandatory$")
	public void i_check_that_something_is_mandatory(String arg1) throws Throwable {

		try {
			System.out.println("Value is " + arg1);
			Thread.sleep(2000);
			String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + arg1 + "'}).mandatory")
					.toString();
			System.out.println("Value of a is " + a);
			if (a.equals("true")) {
				System.out.println("Value of mandatory is" + a);
				assertTrue(true, "value is not mandatory");
			}
		} catch (Exception e) {
			assertTrue(false, e.getLocalizedMessage());
		}
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(
				"C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot1.png"));

		// driver.close();
	}

	@When("^I initiate \"([^\"]*)\" from action button$")
	public void i_initiate_from_action_button(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		driver.findElement(By.xpath("//div[@class='fen-subtitlelinkcontent']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[@class='icon fen-icon-ellipsis undefined']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Maintenance Request']")).click();

	}

	@Then("^I can see product is visible in Product grid$")
	public void i_can_see_product_is_visible_in_product_grid() throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Screen= 'CaptureRequestDetails' and ObjectKey='ProductGridExpand'";
		ResultSet rst = sql.GetDataSet(query);
//		ResultSet rst1 = sql.getControlsData("Controls1", "ProductGridExpand");
		String query1 = "select * from Controls1 where Name= 'CaptureRequestDetails' and ObjectKey='ProductExpand'";
		ResultSet rst1 = sql.GetDataSet(query1);

		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		
		Thread.sleep(5000);
		driver.findElement(By.xpath(locator)).click();
		Thread.sleep(3000);

		Boolean isPresent = driver.findElements(By.xpath(locator1)).size() < 2;
		Assert.assertEquals(true, isPresent);
	
	}

	@Then("^I can see product is visible in Product grid for \"([^\"]*)\"$")
	public void i_can_see_product_is_visible_in_product_grid_for_Screen(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Screen= '" + strArg1 + "' and ObjectKey='ProductGridExpand'";
		ResultSet rst = sql.GetDataSet(query);
//		ResultSet rst1 = sql.getControlsData("Controls1", "ProductGridExpand");
		String query1 = "select * from Controls1 where Name=  '" + strArg1 + "' and ObjectKey='ProductExpand'";
		ResultSet rst1 = sql.GetDataSet(query1);
		String name = rst.getString("Name");
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		while (rst.next()) {
			if (name.equals("ValidateKYCRequest")) {
				continue;
			} else {
				Thread.sleep(2000);
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			}
		}
		Boolean isPresent = driver.findElements(By.xpath(locator1)).size() < 2;
		Assert.assertEquals(true, isPresent);
		Thread.sleep(3000);
	}

	@AfterClass(alwaysRun = true)
	public void screenShot(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {

			try {
				TakesScreenshot screenshot = (TakesScreenshot) driver;
				File src = screenshot.getScreenshotAs(OutputType.FILE);

				// File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(
						"C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot.png"));
				System.out.println("Taken Screenshots");
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}
		}
		driver.quit();

	}

	@And("^I validate the following fields in \"([^\"]*)\" Sub Flow$")
	public void i_validate_the_following_fields_in_sub_flow(String Arg1, DataTable table) throws Throwable {
		
		List<Map<String, String>> list = table.asMaps(String.class, String.class);
		for (int i = 0; i < list.size(); i++) {
			System.out.println("List sie is "+list.size());
			try {
			String label = list.get(i).get("Label").trim();
			String fieldType = list.get(i).get("FieldType").trim();
			String visible = list.get(i).get("Visible").trim();
			String editable = list.get(i).get("ReadOnly").trim();
			String mandatory = list.get(i).get("Mandatory").trim();
			String defaultsto = list.get(i).get("DefaultsTo").trim();
			if (fieldType.equals("Numeric") || (fieldType.equals("Alphanumeric"))) {
				continue;

			} else {
				
				String FieldType = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).type")
						.toString();
				System.out.println("FieldType is  "+FieldType);
				Assert.assertEquals(FieldType, fieldType);

			}
			String locator = "//label[text()='" + label + "']";
			Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
			String Visible = isPresent.toString();
			System.out.println(label + "is" + Visible);

			Assert.assertEquals(Visible, visible);
			String Editable = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).readOnly")
					.toString();
			System.out.println(label + "is Editable as" + Editable);
			Assert.assertEquals(editable, Editable);
			if (label.equals("Is Primary Contact")) {
				continue;
			} else {
				String Mandatory = jsExecutor
						.executeScript("return PageObjects.find({caption:'" + label + "'}).mandatory").toString();
				if(Mandatory==null) {
					Assert.assertEquals(null, Mandatory);
					
				}else {
				Assert.assertEquals(mandatory, Mandatory);
				}
			}

			if (defaultsto.equals("NA") || defaultsto.equals("Select...")) {
				continue;
			} else {
				String value = "//div[text()='" + defaultsto + "']";
				System.out.println("//div[text()='" + defaultsto + "']");
				Boolean IsPresent = driver.findElements(By.xpath(value)).size() > 0;
				System.out.println("value of present is" + IsPresent);
				String DefaultsTo = driver.findElement(By.xpath(value)).getText().toString();
				System.out.println("Value of defaults to" + DefaultsTo);
				Assert.assertEquals(defaultsto, DefaultsTo);

			}

		}
			
		catch(NullPointerException e) {
			
		}

	}}
	@And("^I check that below data is available$")
    public void i_check_that_below_data_is_available(DataTable table) throws Throwable {
		List<Map<String, String>> list = table.asMaps(String.class, String.class);
		for (int i = 0; i < list.size(); i++) {
			try {
				String label = list.get(i).get("Label").trim();
				String value = list.get(i).get("Value").trim();
				if (value.equals("Resident")) {
				String Getvalue = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()")
						.toString();
				System.out.println("Value is  "+Getvalue);
				Assert.assertEquals("700290", Getvalue);
				}
				else if(value.equals("Non-Resident")) {
					String Getvalue = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()")
							.toString();
					System.out.println("Value is  "+Getvalue);
					Assert.assertEquals("700291", Getvalue);
					
				}
			}
			catch(Exception e) {
				
			}
			
			
		}
        
    }


	@And("^I navigate to \"([^\"]*)\" screen by clicking on \"([^\"]*)\" button from \"([^\"]*)\"$")
	public void i_navigate_to_something_screen_by_clicking_on_something_button(String strArg1, String strArg2,
			String strArg3) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='" + strArg2 + "' and name ='" + strArg1
				+ "' and Screen='" + strArg3 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(10000);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
		}
	}

	@And("^I complete \"([^\"]*)\" with Key \"([^\"]*)\" and below data$")
	public void i_complete_capturerequestchanges_with_keyand_below_data(String Tablename, String Datakey,
			DataTable table) throws Throwable {

		try {
			List<Map<String, String>> list = table.asMaps(String.class, String.class);
			for (int i = 0; i < list.size(); i++) {
				String Product = list.get(i).get("Product");
				System.out.println("Product is" + Product);
				String Relationship = list.get(i).get("Relationship");
				SqliteConnection sql = SqliteConnection.getInstance();
				ResultSet rst = sql.buildQuery("Product", Product);
				FillInDataWithoutValidation(rst);
				ResultSet rst1 = sql.buildQuery("Relationship", Relationship);
				Thread.sleep(5000);
				FillInDataWithoutValidation(rst1);
				ResultSet rst2 = sql.buildQuery(Tablename, Datakey);
				Thread.sleep(5000);
				FillInDataWithoutValidation(rst2);

			}
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}

	}

	@And("^I take the screenshot for \"([^\"]*)\"$")
	public void i_take_the_screenshot(String filename) throws Throwable {
		String location = "src\\test\\resources\\";
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String timestamp = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File dest = new File(location + filename + timestamp + ".jpg");
		FileUtils.copyFile(src, dest);

	}
}
